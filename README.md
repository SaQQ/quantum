# Quantum Vortices analyzer

### Project structure 

* Directory **qvortlib/** - Main library project, extensively documented in-code
* Directory **qvort/** - Example application linking to **qvortlib** for demonstration
* Directory **dependencies/** - External libraries used by project (currently only for testing)   

### Building

Project uses CMake build system and was tested with gcc 8.2.1 compiler on Linux x64 platform.

```bash
# Current directory is assumed to contain the source tree
mkdir -p build && build
cmake -DCMAKE_BUILD_TYPE="Release" ..
cmake --build . --target install
# Optionally, build target 'package' for generating archive containing headers and binaries
```

Building target install will try to install the library in default system directories (e.g. /usr/lib).
To override the defaults specify option `-DCMAKE_INSTALL_PREFIX="<install dir>"` prior to generation.
