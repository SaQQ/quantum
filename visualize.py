import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Line3DCollection
import numpy as np
import matplotlib.pyplot as plt

with open("geometry.txt") as file:
    lines = file.readlines()

lines = [map(lambda s: float(s), line.split()) for line in lines]

x, y, z = map(list, zip(*lines))

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.set_aspect('equal')

ax.plot(x, y, z, '.r', markersize=1)

plt.show()
