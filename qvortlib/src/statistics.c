#include <qvortlib/statistics.h>

#include <qvortlib/graph.h>
#include <qvortlib/graph_algo.h>

#include "error.h"

size_t qv_split_vortices(const qv_geometry* geometry) {
    qv_graph graph;
    qv_graph_init(&graph, geometry->vertex_count);

    for (size_t i = 0; (i + 1) < geometry->index_count; i += 2) {
        size_t i1 = geometry->index_buffer[i];
        size_t i2 = geometry->index_buffer[i + 1];
        qv_graph_add_bi_edge(&graph, i1, i2);
    }

    size_t* components = (size_t*)malloc(graph.size * sizeof(size_t));
    if (components == NULL) SYSTEM_ERR("Unable to allocate components memory");
    size_t components_count = qv_connected_components(&graph, components);
    free(components);

    qv_graph_free(&graph);

    return components_count;
}

double qv_vortex_length(const qv_geometry* geometry) {
    double sum = 0.0;

    for (size_t i = 0; (i + 1) < geometry->index_count; i += 2) {
        const qv_vec3* v1 = &geometry->vertex_buffer[geometry->index_buffer[i]];
        const qv_vec3* v2 = &geometry->vertex_buffer[geometry->index_buffer[i + 1]];

        qv_vec3 diff = *v1;
        qv_vec3_sub(&diff, v2);
        sum += qv_vec3_length(&diff);
    }

    return sum;
}


