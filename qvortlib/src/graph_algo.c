#include <qvortlib/graph_algo.h>

#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "error.h"

typedef struct qv_stack_t {
    size_t size;
    size_t capacity;
    size_t* data;
} qv_stack;

void qv_stack_init(qv_stack* stack) {
    assert(stack != NULL);

    stack->size = 0;
    stack->capacity = 1;
    stack->data = (size_t*)malloc(stack->capacity * sizeof(size_t));
    if (stack->data == NULL) SYSTEM_ERR("Unable to allocate stack memory");
}

void qv_stack_push(qv_stack* stack, size_t value) {
    assert(stack != NULL);
    if (stack->size == stack->capacity) {
        stack->capacity *= 2;
        stack->data = (size_t*)realloc(stack->data, stack->capacity * sizeof(size_t));
        if (stack->data == NULL) SYSTEM_ERR("Unable to allocate stack memory");
    }
    stack->data[stack->size] = value;
    stack->size++;
}

size_t qv_stack_pop(qv_stack* stack) {
    assert(stack != NULL);
    assert(stack->size > 0);

    stack->size--;

    if (stack->capacity > 1 && stack->size < stack->capacity / 2) {
        stack->capacity /= 2;
        stack->data = (size_t*)realloc(stack->data, stack->capacity * sizeof(size_t));
        if (stack->data == NULL) SYSTEM_ERR("Unable to allocate stack memory");
    }

    return stack->data[stack->size];
}

int qv_stack_empty(const qv_stack* stack) {
    assert(stack != NULL);

    return stack->size == 0;
}

void qv_stack_free(qv_stack* stack) {
    assert(stack != NULL);

    stack->size = 0;
    stack->capacity = 0;
    free(stack->data);
    stack->data = NULL;
}

size_t qv_connected_components(const qv_graph* graph, size_t* component) {
    unsigned char* visited = (unsigned char*)malloc(graph->size * sizeof(unsigned char));
    if (visited == NULL) SYSTEM_ERR("Unable to allocate memory");
    memset(visited, 0, graph->size * sizeof(unsigned char));

    size_t component_numerator = 0;

    qv_stack stack;
    qv_stack_init(&stack);

    for (size_t v = 0; v < graph->size; ++v) {
        if (visited[v] == 1)
            continue;

        qv_stack_push(&stack, v);

        while (!qv_stack_empty(&stack)) {
            size_t current = qv_stack_pop(&stack);

            if (visited[current] == 1) continue;
            visited[current] = 1;
            component[current] = component_numerator;

            const qv_edge* e = qv_graph_out_edges(graph, current);
            while(e != NULL) {
                qv_stack_push(&stack, e->to);
                e = e->next;
            }

        }

        component_numerator++;
    }

    qv_stack_free(&stack);

    free(visited);

    return component_numerator;
}

