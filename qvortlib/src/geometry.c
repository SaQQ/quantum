#include <qvortlib/geometry.h>

#include <assert.h>
#include <stdlib.h>

#include "error.h"

void qv_geometry_init(qv_geometry* geometry) {
    assert(geometry != NULL);

    geometry->vertex_count = 0;
    geometry->vertex_buffer = (qv_vec3*) malloc(1 * sizeof(qv_vec3));
    if (geometry->vertex_buffer == NULL) SYSTEM_ERR("Unable to allocate vertex buffer memory");
    geometry->vertex_buffer_size = 1;

    geometry->index_count = 0;
    geometry->index_buffer = (size_t*) malloc(1 * sizeof(size_t));
    if (geometry->index_buffer == NULL) SYSTEM_ERR("Unable to allocate index buffer memory");
    geometry->index_buffer_size = 1;
}

void qv_geometry_free(qv_geometry* geometry) {
    assert(geometry != NULL);

    geometry->vertex_count = 0;
    free(geometry->vertex_buffer);
    geometry->vertex_buffer = NULL;
    geometry->vertex_buffer_size = 0;

    geometry->index_count = 0;
    free(geometry->index_buffer);
    geometry->index_buffer = NULL;
    geometry->vertex_buffer_size = 0;
}

size_t qv_geometry_append_vertex(qv_geometry* geometry, const qv_vec3* vertex) {
    if (geometry->vertex_count == geometry->vertex_buffer_size) {
        geometry->vertex_buffer_size *= 2;
        geometry->vertex_buffer = realloc(geometry->vertex_buffer, geometry->vertex_buffer_size * sizeof(qv_vec3));
    }
    geometry->vertex_buffer[geometry->vertex_count] = *vertex;
    geometry->vertex_count++;
    return geometry->vertex_count - 1;
}

void qv_geometry_append_index(qv_geometry* geometry, size_t idx) {
    assert(geometry != NULL);
    assert(idx < geometry->vertex_count);

    if (geometry->index_count == geometry->index_buffer_size) {
        geometry->index_buffer_size *= 2;
        geometry->index_buffer = realloc(geometry->index_buffer, geometry->index_buffer_size * sizeof(size_t));
    }
    geometry->index_buffer[geometry->index_count] = idx;
    geometry->index_count++;
}
