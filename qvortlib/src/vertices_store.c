#include <qvortlib/vertices_store.h>

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "error.h"

static size_t internal_index(const qv_vertices_store* store, size_t x, size_t y, size_t z) {
    return x * qv_vertices_store_y_size(store) * qv_vertices_store_z_size(store) + y * qv_vertices_store_z_size(store) +
           z;
}

static size_t x_face_index(const qv_vertices_store* store, size_t x, size_t y, size_t z) {
    return x * qv_vertices_store_y_size(store) * qv_vertices_store_z_size(store) + y * qv_vertices_store_z_size(store) +
           z;
}

static size_t y_face_index(const qv_vertices_store* store, size_t x, size_t y, size_t z) {
    return x * (qv_vertices_store_y_size(store) + 1) * qv_vertices_store_z_size(store) +
           y * qv_vertices_store_z_size(store) + z;
}

static size_t z_face_index(const qv_vertices_store* store, size_t x, size_t y, size_t z) {
    return x * qv_vertices_store_y_size(store) * (qv_vertices_store_z_size(store) + 1) +
           y * (qv_vertices_store_z_size(store) + 1) + z;
}

void qv_vertices_store_init(qv_vertices_store* store, const qv_grid* grid) {
    assert(grid != NULL);

    store->grid = grid;

    size_t x = grid->x_size - 1;
    size_t y = grid->y_size - 1;
    size_t z = grid->z_size - 1;

    store->internal_indices = (qv_idx_opt_t*) malloc(x * y * z * sizeof(qv_idx_opt_t));
    if (store->internal_indices == NULL) SYSTEM_ERR("Unable to allocate store memory");
    memset(store->internal_indices, 0, x * y * z * sizeof(qv_idx_opt_t));

    store->x_face_indices = (qv_idx_opt_t*) malloc((x + 1) * y * z * sizeof(qv_idx_opt_t));
    if (store->x_face_indices == NULL) SYSTEM_ERR("Unable to allocate store memory");
    memset(store->x_face_indices, 0, (x + 1) * y * z * sizeof(qv_idx_opt_t));

    store->y_face_indices = (qv_idx_opt_t*) malloc(x * (y + 1) * z * sizeof(qv_idx_opt_t));
    if (store->y_face_indices == NULL) SYSTEM_ERR("Unable to allocate store memory");
    memset(store->y_face_indices, 0, x * (y + 1) * z * sizeof(qv_idx_opt_t));

    store->z_face_indices = (qv_idx_opt_t*) malloc(x * y * (z + 1) * sizeof(qv_idx_opt_t));
    if (store->z_face_indices == NULL) SYSTEM_ERR("Unable to allocate store memory");
    memset(store->z_face_indices, 0, x * y * (z + 1) * sizeof(qv_idx_opt_t));

    qv_geometry_init(&store->geometry);
}

void qv_vertices_store_free(qv_vertices_store* store) {
    assert(store != NULL);

    store->grid = NULL;
    free(store->internal_indices);
    store->internal_indices = NULL;
    free(store->x_face_indices);
    store->x_face_indices = NULL;
    free(store->y_face_indices);
    store->y_face_indices = NULL;
    free(store->z_face_indices);
    store->z_face_indices = NULL;

    qv_geometry_free(&store->geometry);
}

size_t qv_vertices_store_x_size(const qv_vertices_store* store) {
    return store->grid->x_size - 1;
}

size_t qv_vertices_store_y_size(const qv_vertices_store* store) {
    return store->grid->y_size - 1;
}

size_t qv_vertices_store_z_size(const qv_vertices_store* store) {
    return store->grid->z_size - 1;
}

size_t qv_vertices_store_capacity(const qv_vertices_store* store) {
    size_t x = store->grid->x_size - 1;
    size_t y = store->grid->y_size - 1;
    size_t z = store->grid->z_size - 1;

    return 4 * x * y * z + x * y + y * z + z * x;
}

void set_vertex(qv_vertices_store* store, qv_idx_opt_t* index, const qv_vec3* vertex) {
    if (QV_IDX_HAS_VALUE(*index)) {
        store->geometry.vertex_buffer[QV_IDX_VALUE(*index)] = *vertex;
    } else {
        size_t idx = qv_geometry_append_vertex(&store->geometry, vertex);
        *index = QV_IDX_MAKE_VALUE(idx);
    }
}

// TODO: x, y, z assertions

size_t qv_vertices_store_set_internal_vertex(qv_vertices_store* store, size_t x, size_t y, size_t z,
                                             const qv_vec3* vertex) {
    assert(store != NULL);
    assert(vertex != NULL);

    set_vertex(store, &store->internal_indices[internal_index(store, x, y, z)], vertex);
    return QV_IDX_VALUE(store->internal_indices[internal_index(store, x, y, z)]);
}

size_t qv_vertices_store_set_x_face_vertex(qv_vertices_store* store, size_t x, size_t y, size_t z,
                                           const qv_vec3* vertex) {
    assert(store != NULL);
    assert(vertex != NULL);

    set_vertex(store, &store->x_face_indices[x_face_index(store, x, y, z)], vertex);
    return QV_IDX_VALUE(store->x_face_indices[x_face_index(store, x, y, z)]);
}

size_t qv_vertices_store_set_y_face_vertex(qv_vertices_store* store, size_t x, size_t y, size_t z,
                                           const qv_vec3* vertex) {
    assert(store != NULL);
    assert(vertex != NULL);

    set_vertex(store, &store->y_face_indices[y_face_index(store, x, y, z)], vertex);
    return QV_IDX_VALUE(store->y_face_indices[y_face_index(store, x, y, z)]);
}

size_t qv_vertices_store_set_z_face_vertex(qv_vertices_store* store, size_t x, size_t y, size_t z,
                                           const qv_vec3* vertex) {
    assert(store != NULL);
    assert(vertex != NULL);

    set_vertex(store, &store->z_face_indices[z_face_index(store, x, y, z)], vertex);
    return QV_IDX_VALUE(store->z_face_indices[z_face_index(store, x, y, z)]);
}

size_t qv_vertices_store_set_face_vertex(qv_vertices_store* store, size_t x, size_t y, size_t z,
                                         qv_face_type type, const qv_vec3* vertex) {
    switch (type) {
        case QV_FACE_X_NEG:
            return qv_vertices_store_set_x_face_vertex(store, x, y, z, vertex);
        case QV_FACE_X_POS:
            return qv_vertices_store_set_x_face_vertex(store, x + 1, y, z, vertex);
        case QV_FACE_Y_NEG:
            return qv_vertices_store_set_y_face_vertex(store, x, y, z, vertex);
        case QV_FACE_Y_POS:
            return qv_vertices_store_set_y_face_vertex(store, x, y + 1, z, vertex);
        case QV_FACE_Z_NEG:
            return qv_vertices_store_set_z_face_vertex(store, x, y, z, vertex);
        case QV_FACE_Z_POS:
            return qv_vertices_store_set_z_face_vertex(store, x, y, z + 1, vertex);
        default:
            assert(0);
    }
}

// TODO: x, y, z assertions

int qv_vertices_store_get_internal_vertex(const qv_vertices_store* store, size_t x, size_t y, size_t z,
                                          qv_vec3* vertex, size_t* index) {
    assert(store != NULL);
    assert(vertex != NULL);
    assert(index != NULL);

    qv_idx_opt_t idx = store->internal_indices[internal_index(store, x, y, z)];
    if (QV_IDX_HAS_VALUE(idx)) {
        *vertex = store->geometry.vertex_buffer[QV_IDX_VALUE(idx)];
        *index = QV_IDX_VALUE(idx);
        return 1;
    } else {
        return 0;
    }
}

int qv_vertices_store_get_x_face_vertex(const qv_vertices_store* store, size_t x, size_t y, size_t z,
                                        qv_vec3* vertex, size_t* index) {
    assert(store != NULL);
    assert(vertex != NULL);
    assert(index != NULL);

    qv_idx_opt_t idx = store->x_face_indices[x_face_index(store, x, y, z)];
    if (QV_IDX_HAS_VALUE(idx)) {
        *vertex = store->geometry.vertex_buffer[QV_IDX_VALUE(idx)];
        *index = QV_IDX_VALUE(idx);
        return 1;
    } else {
        return 0;
    }
}

int qv_vertices_store_get_y_face_vertex(const qv_vertices_store* store, size_t x, size_t y, size_t z,
                                        qv_vec3* vertex, size_t* index) {
    assert(store != NULL);
    assert(vertex != NULL);
    assert(index != NULL);

    qv_idx_opt_t idx = store->y_face_indices[y_face_index(store, x, y, z)];
    if (QV_IDX_HAS_VALUE(idx)) {
        *vertex = store->geometry.vertex_buffer[QV_IDX_VALUE(idx)];
        *index = QV_IDX_VALUE(idx);
        return 1;
    } else {
        return 0;
    }
}

int qv_vertices_store_get_z_face_vertex(const qv_vertices_store* store, size_t x, size_t y, size_t z,
                                        qv_vec3* vertex, size_t* index) {
    assert(store != NULL);
    assert(vertex != NULL);
    assert(index != NULL);

    qv_idx_opt_t idx = store->z_face_indices[z_face_index(store, x, y, z)];
    if (QV_IDX_HAS_VALUE(idx)) {
        *vertex = store->geometry.vertex_buffer[QV_IDX_VALUE(idx)];
        *index = QV_IDX_VALUE(idx);
        return 1;
    } else {
        return 0;
    }
}

int qv_vertices_store_get_face_vertex(const qv_vertices_store* store, size_t x, size_t y, size_t z,
                                      qv_face_type type, qv_vec3* vertex, size_t* index) {
    switch (type) {
        case QV_FACE_X_NEG:
            return qv_vertices_store_get_x_face_vertex(store, x, y, z, vertex, index);
        case QV_FACE_X_POS:
            return qv_vertices_store_get_x_face_vertex(store, x + 1, y, z, vertex, index);
        case QV_FACE_Y_NEG:
            return qv_vertices_store_get_y_face_vertex(store, x, y, z, vertex, index);
        case QV_FACE_Y_POS:
            return qv_vertices_store_get_y_face_vertex(store, x, y + 1, z, vertex, index);
        case QV_FACE_Z_NEG:
            return qv_vertices_store_get_z_face_vertex(store, x, y, z, vertex, index);
        case QV_FACE_Z_POS:
            return qv_vertices_store_get_z_face_vertex(store, x, y, z + 1, vertex, index);
        default:
            assert(0);
    }
}

void qv_vertices_store_append_index(qv_vertices_store* store, size_t idx) {
    qv_geometry_append_index(&store->geometry, idx);
}
