#include <qvortlib/extractor.h>

#include <assert.h>
#include <stdlib.h>

#include <qvortlib/vertices_store.h>
#include <qvortlib/solver.h>

static int generate_face_vertex(const qv_face* face, qv_vec3* vertex) {
    assert(face != NULL);
    assert(vertex != NULL);

    qv_complex deltas[4];

    qv_face_get_delta(face, 0, 0, &deltas[0]);
    qv_face_get_delta(face, 0, 1, &deltas[1]);
    qv_face_get_delta(face, 1, 0, &deltas[2]);
    qv_face_get_delta(face, 1, 1, &deltas[3]);

    int vorticity = qv_get_vorticity(deltas);

    if (vorticity != 0) {
        double u, v;
        if (qv_solve_bilinear_interpolation(deltas, &u, &v) != -1) {
            qv_face_get_internal_point(face, u, v, vertex);
            return 1;
        } else {
            qv_face_get_internal_point(face, 0.5, 0.5, vertex);
            return 1;
        }
    } else {
        return 0;
    }
}

static void generate_cube_vertex(const qv_cube* cube, qv_vec3* vertex) {
    assert(cube != NULL);
    assert(vertex != NULL);

    qv_cube_get_internal_point(cube, 0.5, 0.5, 0.5, vertex);
}

void qv_generate_vertices(qv_vertices_store* store) {
    const qv_grid* grid = store->grid;

    for (size_t x = 0; x < grid->x_size - 1; ++x) {
        for (size_t y = 0; y < grid->y_size - 1; ++y) {
            for (size_t z = 0; z < grid->z_size - 1; ++z) {

                int has_x_neg_vertex, has_y_neg_vertex, has_z_neg_vertex, has_x_pos_vertex, has_y_pos_vertex, has_z_pos_vertex;
                qv_vec3 x_neg_vertex, y_neg_vertex, z_neg_vertex, x_pos_vertex, y_pos_vertex, z_pos_vertex;
                size_t x_neg_index, y_neg_index, z_neg_index, x_pos_index, y_pos_index, z_pos_index;

                if (x == 0) {
                    qv_face face;
                    qv_get_face(grid, x, y, z, QV_FACE_X_NEG, &face);
                    has_x_neg_vertex = generate_face_vertex(&face, &x_neg_vertex);
                    if (has_x_neg_vertex) {
                        x_neg_index = qv_vertices_store_set_face_vertex(store, x, y, z, QV_FACE_X_NEG, &x_neg_vertex);
                    }
                } else {
                    has_x_neg_vertex = qv_vertices_store_get_face_vertex(store, x, y, z, QV_FACE_X_NEG, &x_neg_vertex,
                                                                         &x_neg_index);
                }

                if (y == 0) {
                    qv_face face;
                    qv_get_face(grid, x, y, z, QV_FACE_Y_NEG, &face);
                    has_y_neg_vertex = generate_face_vertex(&face, &y_neg_vertex);
                    if (has_y_neg_vertex) {
                        y_neg_index = qv_vertices_store_set_face_vertex(store, x, y, z, QV_FACE_Y_NEG, &y_neg_vertex);
                    }
                } else {
                    has_y_neg_vertex = qv_vertices_store_get_face_vertex(store, x, y, z, QV_FACE_Y_NEG, &y_neg_vertex,
                                                                         &y_neg_index);
                }

                if (z == 0) {
                    qv_face face;
                    qv_get_face(grid, x, y, z, QV_FACE_Z_NEG, &face);
                    has_z_neg_vertex = generate_face_vertex(&face, &z_neg_vertex);
                    if (has_z_neg_vertex) {
                        z_neg_index = qv_vertices_store_set_face_vertex(store, x, y, z, QV_FACE_Z_NEG, &z_neg_vertex);
                    }
                } else {
                    has_z_neg_vertex = qv_vertices_store_get_face_vertex(store, x, y, z, QV_FACE_Z_NEG, &z_neg_vertex,
                                                                         &z_neg_index);
                }

                qv_face x_pos_face, y_pos_face, z_pos_face;
                qv_get_face(grid, x, y, z, QV_FACE_X_POS, &x_pos_face);
                qv_get_face(grid, x, y, z, QV_FACE_Y_POS, &y_pos_face);
                qv_get_face(grid, x, y, z, QV_FACE_Z_POS, &z_pos_face);

                has_x_pos_vertex = generate_face_vertex(&x_pos_face, &x_pos_vertex);
                if (has_x_pos_vertex) {
                    x_pos_index = qv_vertices_store_set_face_vertex(store, x, y, z, QV_FACE_X_POS, &x_pos_vertex);
                }

                has_y_pos_vertex = generate_face_vertex(&y_pos_face, &y_pos_vertex);
                if (has_y_pos_vertex) {
                    y_pos_index = qv_vertices_store_set_face_vertex(store, x, y, z, QV_FACE_Y_POS, &y_pos_vertex);
                }

                has_z_pos_vertex = generate_face_vertex(&z_pos_face, &z_pos_vertex);
                if (has_z_pos_vertex) {
                    z_pos_index = qv_vertices_store_set_face_vertex(store, x, y, z, QV_FACE_Z_POS, &z_pos_vertex);
                }

                int face_vertices_count = has_x_neg_vertex + has_y_neg_vertex + has_z_neg_vertex +
                                          has_x_pos_vertex + has_y_pos_vertex + has_z_pos_vertex;

                if (face_vertices_count == 2) {
                    // connect these two vertices

                    if (has_x_neg_vertex) {
                        qv_vertices_store_append_index(store, x_neg_index);
                    }

                    if (has_y_neg_vertex) {
                        qv_vertices_store_append_index(store, y_neg_index);
                    }

                    if (has_z_neg_vertex) {
                        qv_vertices_store_append_index(store, z_neg_index);
                    }

                    if (has_x_pos_vertex) {
                        qv_vertices_store_append_index(store, x_pos_index);
                    }

                    if (has_y_pos_vertex) {
                        qv_vertices_store_append_index(store, y_pos_index);
                    }

                    if (has_z_pos_vertex) {
                        qv_vertices_store_append_index(store, z_pos_index);
                    }

                } else if (face_vertices_count > 2) {
                    // connect all vertices with the internal one

                    size_t internal_index;
                    qv_vec3 internal_vertex;

                    qv_cube cube;
                    qv_get_cube(grid, x, y, z, &cube);
                    generate_cube_vertex(&cube, &internal_vertex);
                    internal_index = qv_vertices_store_set_internal_vertex(store, x, y, z, &internal_vertex);

                    if (has_x_neg_vertex) {
                        qv_vertices_store_append_index(store, internal_index);
                        qv_vertices_store_append_index(store, x_neg_index);
                    }

                    if (has_y_neg_vertex) {
                        qv_vertices_store_append_index(store, internal_index);
                        qv_vertices_store_append_index(store, y_neg_index);
                    }

                    if (has_z_neg_vertex) {
                        qv_vertices_store_append_index(store, internal_index);
                        qv_vertices_store_append_index(store, z_neg_index);
                    }

                    if (has_x_pos_vertex) {
                        qv_vertices_store_append_index(store, internal_index);
                        qv_vertices_store_append_index(store, x_pos_index);
                    }

                    if (has_y_pos_vertex) {
                        qv_vertices_store_append_index(store, internal_index);
                        qv_vertices_store_append_index(store, y_pos_index);
                    }

                    if (has_z_pos_vertex) {
                        qv_vertices_store_append_index(store, internal_index);
                        qv_vertices_store_append_index(store, z_pos_index);
                    }
                }

            }
        }
    }

}
