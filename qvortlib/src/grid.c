#include <qvortlib/grid.h>

#include <stdlib.h>
#include <assert.h>

#include "error.h"

void qv_grid_init(qv_grid* grid, size_t x, size_t y, size_t z, double d_x, double d_y, double d_z) {
    assert(grid != NULL);
    assert(x > 0);
    assert(y > 0);
    assert(z > 0);
    assert(d_x > 0.0);
    assert(d_y > 0.0);
    assert(d_z > 0.0);

    grid->x_size = x;
    grid->y_size = y;
    grid->z_size = z;
    grid->d_x = d_x;
    grid->d_y = d_y;
    grid->d_z = d_z;
    grid->deltas = (qv_complex*)malloc(x * y * z * sizeof(qv_complex));
    if (grid->deltas == NULL) SYSTEM_ERR("Unable to allocate grid memory");
}

void qv_grid_free(qv_grid *grid) {
    assert(grid != NULL);

    grid->x_size = 0;
    grid->y_size = 0;
    grid->z_size = 0;
    grid->d_x = 0.0;
    grid->d_y = 0.0;
    grid->d_z = 0.0;
    free(grid->deltas);
    grid->deltas = NULL;
}

static size_t grid_index(const qv_grid* grid, size_t x, size_t y, size_t z) {
    return x * grid->y_size * grid->z_size + y * grid->z_size + z;
}

void qv_grid_get_delta(const qv_grid *grid, size_t x, size_t y, size_t z, qv_complex *delta) {
    *delta = grid->deltas[grid_index(grid, x, y, z)];
}

void gv_grid_set_delta(qv_grid *grid, size_t x, size_t y, size_t z, qv_complex *value) {
    grid->deltas[grid_index(grid, x, y, z)] = *value;
}

void qv_grid_get_point(const qv_grid *grid, size_t x, size_t y, size_t z, qv_vec3 *point) {
    point->x = x * grid->d_x;
    point->y = y * grid->d_y;
    point->z = z * grid->d_z;
}
