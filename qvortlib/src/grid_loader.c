#include <qvortlib/grid_loader.h>

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include "error.h"

// TODO: Informative error messages

void qv_read_binary_file(const char* path, qv_grid* grid, size_t frame_index) {
    FILE* fp = fopen(path, "rb");
    if (fp == NULL) SYSTEM_ERR("File IO error");

    uint32_t _dummy_i, x_size, y_size, z_size, frames_count;
    double _dummy_d, d_x, d_y, d_z;

    if (fread(&_dummy_i, sizeof(_dummy_i), 1, fp) != 1) SYSTEM_ERR("File IO error");

    if (fread(&z_size, sizeof(z_size), 1, fp) != 1) SYSTEM_ERR("File IO error");
    if (fread(&y_size, sizeof(y_size), 1, fp) != 1) SYSTEM_ERR("File IO error");
    if (fread(&x_size, sizeof(x_size), 1, fp) != 1) SYSTEM_ERR("File IO error");

    if (fread(&d_z, sizeof(d_z), 1, fp) != 1) SYSTEM_ERR("File IO error");
    if (fread(&d_y, sizeof(d_y), 1, fp) != 1) SYSTEM_ERR("File IO error");
    if (fread(&d_x, sizeof(d_x), 1, fp) != 1) SYSTEM_ERR("File IO error");

    if (fread(&_dummy_d, sizeof(_dummy_d), 1, fp) != 1) SYSTEM_ERR("File IO error");
    if (fread(&_dummy_d, sizeof(_dummy_d), 1, fp) != 1) SYSTEM_ERR("File IO error");
    if (fread(&_dummy_d, sizeof(_dummy_d), 1, fp) != 1) SYSTEM_ERR("File IO error");

    qv_grid_init(grid, x_size, y_size, z_size, d_x, d_y, d_z);

    if (fread(&frames_count, sizeof(frames_count), 1, fp) != 1) SYSTEM_ERR("File IO error");

    size_t grid_points = grid->x_size * grid->y_size * grid->z_size;

    if (fseek(fp, frame_index * grid_points * 2 * sizeof(double), SEEK_CUR) != 0) SYSTEM_ERR("File IO error");

    qv_complex* p = grid->deltas;

    for (size_t x = 0; x < grid->x_size; ++x) {
        for (size_t y = 0; y < grid->y_size; ++y) {
            for (size_t z = 0; z < grid->z_size; ++z) {
                if (fread(&p->real, sizeof(p->real), 1, fp) != 1) SYSTEM_ERR("File IO error");
                if (fread(&p->imag, sizeof(p->imag), 1, fp) != 1) SYSTEM_ERR("File IO error");
                p++;
            }
        }
    }

    if (fclose(fp) != 0) SYSTEM_ERR("File IO error");
}

void qv_read_text_file(const char* path, qv_grid* grid) {
    PROGRAM_ERR("Text files not yet supported");
}
