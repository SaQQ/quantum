#include <qvortlib/solver.h>

#include <assert.h>
#include <math.h>

static double normalize_angle(double a) {
    a = remainder(a, 2.0 * M_PI);
    return a > M_PI ? (-2.0 * M_PI + a) : a;
}

static const double eps = 0.00000001;

static double is_zero(double x) {
     return fabs(x) < eps;
}

int qv_get_vorticity(qv_complex* deltas) {
    double mag0 = qv_complex_mag(&deltas[0]);
    double mag1 = qv_complex_mag(&deltas[1]);
    double mag2 = qv_complex_mag(&deltas[2]);
    double mag3 = qv_complex_mag(&deltas[3]);

    // TODO: Extract this constant
    const double delta_threshold = 0.001;

    if (mag0 < delta_threshold || mag1 < delta_threshold ||
        mag2 < delta_threshold || mag3 < delta_threshold) {
        return 0;
    }

    double arg0 = qv_complex_arg(&deltas[0]);
    double arg1 = qv_complex_arg(&deltas[1]);
    double arg2 = qv_complex_arg(&deltas[2]);
    double arg3 = qv_complex_arg(&deltas[3]);

    double d_arg1 = normalize_angle(arg0 - arg2);
    double d_arg2 = normalize_angle(arg2 - arg3);
    double d_arg3 = normalize_angle(arg3 - arg1);
    double d_arg4 = normalize_angle(arg1 - arg0);

    double a_sum = d_arg1 + d_arg2 + d_arg3 + d_arg4;

    if (fabs(a_sum) < 0.001) {
        return 0;
    } else {
        double vorticity = a_sum / 2.0 / M_PI;
        return (int)round(vorticity);
    }
}

typedef struct bilinear_equation_t {
    double r[4];
    double i[4];
} bilinear_equation;

static int calculate_v(const bilinear_equation* eq, double u, double* pv) {

    if (u < 0.0 || u > 1.0) {
        return -1;
    }

    double va = (1.0 - u) * (eq->r[2] - eq->r[0]) + u * (eq->r[3] - eq->r[1]);
    double vb = (1.0 - u) * eq->r[0] + u * eq->r[1];
    double v = -1.0;

    if (is_zero(va)) {
        if (is_zero(vb)) {
            *pv = 0.5;
            return 0;
        } else {
            va = (1.0 - u) * (eq->i[2] - eq->i[0]) + u * (eq->i[3] - eq->i[1]);
            vb = (1.0 - u) * eq->i[0] + u * eq->i[1];
            if (is_zero(va)) {
                if (is_zero(vb)) {
                    *pv = 0.5;
                    return 0;
                } else {
                    return -1;
                }
            } else {
                v = -vb / va;
            }
        }
    } else {
        v = -vb / va;
    }

    if (v < 0.0 || v > 1.0) {
        return -1;
    }

    *pv = v;
    return 0;
}

static int handle_degeneration(const double x[4], double* pu, double *pv) {
    /*
     * Domain structure:
     * (r2, i2) --- (r3, i3)
     *    |            |
     *    |            |
     * (r0, i0) --- (r1, i1)
     */

    /* (1 - v)((1 - u) * r0 + u * r1) + v((1-u) * r2 + u * r3) = 0 */

    /* ((1 - u) * r0 + u * r1) + v((1-u) * r2 + u * r3 - (1 - u) * r0 - u * r1)) = 0 */

    /* (r0 + u(r1 - r0)) + v(r2 - r0 + u(r3 - r1 - r2 + r0)) = 0 */

    if (is_zero(x[3] - x[1] - x[2] + x[0])) {
        if (is_zero(x[2] - x[0])) {
            // any u degenerates equation
            if (is_zero(x[1] - x[0])) {
                if (is_zero(x[0])) {
                    // (any u, any v) satisfies equation
                    *pu = 0.5;
                    *pv = 0.5;
                    return 1;
                } else {
                    // system has no solutions
                    return -1;
                }
            } else {
                // (u_zero, any v) satisfies equation
                *pu = -x[0] / (x[1] - x[0]);
                *pv = 0.5;
                return 1;
            }
        } else {
            // no degeneration occurs
            return -1;
        }
    } else {
        // singe u degenerates equation
        double u_zero = (x[2] - x[0]) / (x[3] - x[1] - x[2] + x[0]);
        if (is_zero((1.0 - u_zero) * x[0] + u_zero * x[1])) {
            *pu = u_zero;
            *pv = 0.5;
            return 1;
        } else {
            return -1;
        }
    }
}

static int solve_no_degeneration(const bilinear_equation* eq, double *pu, double *pv) {
    // non degenerated system
    double b0 = eq->r[0] * eq->i[2] - eq->r[2] * eq->i[0];
    double b1 = (eq->r[0] * eq->i[3] + eq->r[1] * eq->i[2] - eq->r[2] * eq->i[1] - eq->r[3] * eq->i[0]) / 2.0;
    double b2 = eq->r[1] * eq->i[3] - eq->r[3] * eq->i[1];

    if (is_zero(b0 - 2.0 * b1 + b2)) {

        if (is_zero(2.0 * (b1 - b0))) {

            if (is_zero(b0)) {
                return -1;
            } else {
                // indefinite solution
                return -1;
            }

        } else {
            // equation is linear
            double u = -b0 / 2.0 / (b1 - b0);

            if (calculate_v(eq, u, pv) != -1) {
                *pu = u;
                return 0;
            }

            return -1;
        }

    } else {
        // equation is quadratic

        // this way delta can be calculated in bernstein basis
        double delta = 4.0 * (b1 * b1 - b0 * b2);

        if (delta >= 0.0) {
            if (is_zero(delta)) {
                double u = (b0 - b1) / (b0 - 2.0 * b1 + b2);

                if (calculate_v(eq, u, pv) != -1) {
                    *pu = u;
                    return 0;
                }

                return -1;

            } else {
                // arbitrarily choose one of solutions
                double u1 = (b0 - b1 - 0.5 * sqrt(delta)) / (b0 - 2.0 * b1 + b2);
                double u2 = (b0 - b1 + 0.5 * sqrt(delta)) / (b0 - 2.0 * b1 + b2);

                if (calculate_v(eq, u1, pv) != -1) {
                    *pu = u1;
                    return 0;
                }

                if (calculate_v(eq, u2, pv) != -1) {
                    *pu = u2;
                    return 0;
                }

                return -1;
            }

        } else {
            return -1;
        }
    }
}

int qv_solve_bilinear_interpolation(qv_complex deltas[4], double* pu, double* pv) {
    assert(pu != NULL);
    assert(pv != NULL);

    bilinear_equation eq;

    eq.r[0] = deltas[0].real;
    eq.r[1] = deltas[1].real;
    eq.r[2] = deltas[2].real;
    eq.r[3] = deltas[3].real;

    eq.i[0] = deltas[0].imag;
    eq.i[1] = deltas[1].imag;
    eq.i[2] = deltas[2].imag;
    eq.i[3] = deltas[3].imag;


    int result = solve_no_degeneration(&eq, pu, pv);
    if (result == 0) {
        return 0;
    }

    result = handle_degeneration(eq.r, pu, pv);
    assert(result == -1);
    result = handle_degeneration(eq.i, pu, pv);
    assert(result == -1);
    return result;
}
