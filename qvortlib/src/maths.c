#include <qvortlib/maths.h>

#include <math.h>

void qv_vec3_add(qv_vec3* lhs, const qv_vec3* rhs) {
    lhs->x += rhs->x;
    lhs->y += rhs->y;
    lhs->z += rhs->z;
}

void qv_vec3_sub(qv_vec3* lhs, const qv_vec3* rhs) {
    lhs->x -= rhs->x;
    lhs->y -= rhs->y;
    lhs->z -= rhs->z;
}

void qv_vec3_mul(qv_vec3* lhs, double rhs) {
    lhs->x *= rhs;
    lhs->y *= rhs;
    lhs->z *= rhs;
}

void qv_vec3_div(qv_vec3* lhs, double rhs) {
    lhs->x /= rhs;
    lhs->y /= rhs;
    lhs->z /= rhs;
}

double qv_vec3_length(const qv_vec3* vec) {
    return sqrt(qv_vec3_length_sq(vec));
}

double qv_vec3_length_sq(const qv_vec3* vec) {
    return vec->x * vec->x +
           vec->y * vec->y +
           vec->z * vec->z;
}

double qv_complex_arg(const qv_complex* c) {
    return atan2(c->imag, c->real);
}

double qv_complex_mag(const qv_complex* c) {
    return sqrt(qv_complex_mag_sq(c));
}

double qv_complex_mag_sq(const qv_complex* c) {
    return c->real * c->real + c->imag * c->imag;
}

void qv_complex_add(qv_complex* lhs, const qv_complex* rhs) {
    lhs->real += rhs->real;
    lhs->imag += rhs->imag;
}

void qv_complex_sub(qv_complex* lhs, const qv_complex* rhs) {
    lhs->real -= rhs->real;
    lhs->imag -= rhs->imag;
}

void qv_complex_mul(qv_complex* lhs, double rhs) {
    lhs->real *= rhs;
    lhs->imag *= rhs;
}

void qv_complex_div(qv_complex* lhs, double rhs) {
    lhs->real /= rhs;
    lhs->imag /= rhs;
}
