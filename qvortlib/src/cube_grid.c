#include <qvortlib/cube_grid.h>

#include <assert.h>

void qv_get_cube(const qv_grid *grid, size_t x, size_t y, size_t z, qv_cube *cube) {
    assert(grid != NULL);
    assert(cube != NULL);
    assert(x >= 0 && x < grid->x_size - 1);
    assert(y >= 0 && y < grid->y_size - 1);
    assert(z >= 0 && z < grid->z_size - 1);

    cube->grid = grid;
    cube->x = x;
    cube->y = y;
    cube->z = z;
}

void qv_cube_get_delta(const qv_cube *cube, int x_pos, int y_pos, int z_pos, qv_complex *delta) {
    assert(cube != NULL);
    assert(delta != NULL);
    assert(x_pos == 0 || x_pos == 1);
    assert(y_pos == 0 || y_pos == 1);
    assert(z_pos == 0 || z_pos == 1);

    qv_grid_get_delta(cube->grid, cube->x + x_pos, cube->y + y_pos, cube->z + z_pos, delta);
}

void qv_cube_get_point(const qv_cube *cube, int x_pos, int y_pos, int z_pos, qv_vec3 *point) {
    assert(cube != NULL);
    assert(point != NULL);
    assert(x_pos == 0 || x_pos == 1);
    assert(y_pos == 0 || y_pos == 1);
    assert(z_pos == 0 || z_pos == 1);

    qv_grid_get_point(cube->grid, cube->x + x_pos, cube->y + y_pos, cube->z + z_pos, point);
}

void qv_cube_get_internal_point(const qv_cube *cube, double x, double y, double z, qv_vec3 *point) {
    assert(cube != NULL);
    assert(point != NULL);
    assert(x >= 0.0 || x <= 1.0);
    assert(y >= 0.0 || y <= 1.0);
    assert(z >= 0.0 || z <= 1.0);

    qv_vec3 p000, p001, p010, p011;
    qv_vec3 p100, p101, p110, p111;

    qv_cube_get_point(cube, 0, 0, 0, &p000);
    qv_cube_get_point(cube, 0, 0, 1, &p001);
    qv_cube_get_point(cube, 0, 1, 0, &p010);
    qv_cube_get_point(cube, 0, 1, 1, &p011);

    qv_cube_get_point(cube, 1, 0, 0, &p100);
    qv_cube_get_point(cube, 1, 0, 1, &p101);
    qv_cube_get_point(cube, 1, 1, 0, &p110);
    qv_cube_get_point(cube, 1, 1, 1, &p111);

    qv_vec3_mul(&p000, (1.0 - x));
    qv_vec3_mul(&p001, (1.0 - x));
    qv_vec3_mul(&p010, (1.0 - x));
    qv_vec3_mul(&p011, (1.0 - x));

    qv_vec3_mul(&p100, x);
    qv_vec3_mul(&p101, x);
    qv_vec3_mul(&p110, x);
    qv_vec3_mul(&p111, x);

    qv_vec3_add(&p000, &p100);
    qv_vec3_add(&p001, &p101);

    qv_vec3_add(&p010, &p110);
    qv_vec3_add(&p011, &p111);

    qv_vec3_mul(&p000, (1.0 - y));
    qv_vec3_mul(&p001, (1.0 - y));

    qv_vec3_mul(&p010, y);
    qv_vec3_mul(&p011, y);

    qv_vec3_add(&p000, &p010);

    qv_vec3_add(&p001, &p011);

    qv_vec3_mul(&p000, (1.0 - z));

    qv_vec3_mul(&p001, z);

    qv_vec3_add(&p000, &p001);

    *point = p000;
}

void qv_get_face(const qv_grid *grid, size_t x, size_t y, size_t z, qv_face_type type, qv_face *face) {
    assert(grid != NULL);
    assert(face != NULL);
    assert(x >= 0 && x < grid->x_size - 1);
    assert(y >= 0 && y < grid->y_size - 1);
    assert(z >= 0 && z < grid->z_size - 1);

    face->grid = grid;
    face->x = x;
    face->y = y;
    face->z = z;
    face->type = type;
}

void qv_get_cube_face(const qv_cube *cube, qv_face_type type, qv_face *face) {
    assert(cube != NULL);
    assert(face != NULL);
    assert(type == QV_FACE_X_NEG ||
           type == QV_FACE_X_POS ||
           type == QV_FACE_Y_NEG ||
           type == QV_FACE_Y_POS ||
           type == QV_FACE_Z_NEG ||
           type == QV_FACE_Z_POS);

    face->grid = cube->grid;
    face->x = cube->x;
    face->y = cube->y;
    face->z = cube->z;
    face->type = type;
}

void qv_face_get_delta(const qv_face *face, int u_pos, int v_pos, qv_complex *delta) {
    assert(face != NULL);
    assert(delta != NULL);
    assert(u_pos == 0 || u_pos == 1);
    assert(v_pos == 0 || v_pos == 1);

    switch (face->type) {
        case QV_FACE_X_NEG:
            qv_grid_get_delta(face->grid, face->x, face->y + u_pos, face->z + v_pos, delta);
            break;
        case QV_FACE_X_POS:
            qv_grid_get_delta(face->grid, face->x + 1, face->y + u_pos, face->z + v_pos, delta);
            break;
        case QV_FACE_Y_NEG:
            qv_grid_get_delta(face->grid, face->x + v_pos, face->y, face->z + u_pos, delta);
            break;
        case QV_FACE_Y_POS:
            qv_grid_get_delta(face->grid, face->x + v_pos, face->y + 1, face->z + u_pos, delta);
            break;
        case QV_FACE_Z_NEG:
            qv_grid_get_delta(face->grid, face->x + u_pos, face->y + v_pos, face->z, delta);
            break;
        case QV_FACE_Z_POS:
            qv_grid_get_delta(face->grid, face->x + u_pos, face->y + v_pos, face->z + 1, delta);
            break;
        default:
            assert(0);
    }
}

void qv_face_get_point(const qv_face *face, int u_pos, int v_pos, qv_vec3 *point) {
    assert(face != NULL);
    assert(point != NULL);
    assert(u_pos == 0 || u_pos == 1);
    assert(v_pos == 0 || v_pos == 1);

    switch (face->type) {
        case QV_FACE_X_NEG:
            qv_grid_get_point(face->grid, face->x, face->y + u_pos, face->z + v_pos, point);
            break;
        case QV_FACE_X_POS:
            qv_grid_get_point(face->grid, face->x + 1, face->y + u_pos, face->z + v_pos, point);
            break;
        case QV_FACE_Y_NEG:
            qv_grid_get_point(face->grid, face->x + v_pos, face->y, face->z + u_pos, point);
            break;
        case QV_FACE_Y_POS:
            qv_grid_get_point(face->grid, face->x + v_pos, face->y + 1, face->z + u_pos, point);
            break;
        case QV_FACE_Z_NEG:
            qv_grid_get_point(face->grid, face->x + u_pos, face->y + v_pos, face->z, point);
            break;
        case QV_FACE_Z_POS:
            qv_grid_get_point(face->grid, face->x + u_pos, face->y + v_pos, face->z + 1, point);
            break;
        default:
            assert(0);
    }
}

void qv_face_get_internal_point(const qv_face *face, double u, double v, qv_vec3 *point) {
    assert(face != NULL);
    assert(point != NULL);
    assert(u >= 0.0 && u <= 1.0);
    assert(v >= 0.0 && v <= 1.0);

    qv_vec3 p00, p01, p10, p11;

    qv_face_get_point(face, 0, 0, &p00);
    qv_face_get_point(face, 0, 1, &p01);
    qv_face_get_point(face, 1, 0, &p10);
    qv_face_get_point(face, 1, 1, &p11);

    qv_vec3_mul(&p00, (1.0 - u));
    qv_vec3_mul(&p01, (1.0 - u));

    qv_vec3_mul(&p10, u);
    qv_vec3_mul(&p11, u);

    qv_vec3_add(&p00, &p10);
    qv_vec3_add(&p01, &p11);

    qv_vec3_mul(&p00, (1.0 - v));
    qv_vec3_mul(&p01, v);

    qv_vec3_add(&p00, &p01);

    *point = p00;
}
