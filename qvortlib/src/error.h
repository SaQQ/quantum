#ifndef QVORTLIB_ERROR_H
#define QVORTLIB_ERROR_H

#include <stdio.h>
#include <stdlib.h>

#define SYSTEM_ERR(msg) (fprintf(stderr, "%s:%d\n", __FILE__, __LINE__), perror(msg), exit(EXIT_FAILURE))
#define PROGRAM_ERR(msg) (fprintf(stderr, "%s:%d\n", __FILE__, __LINE__), fprintf(stderr, msg), exit(EXIT_FAILURE))

#endif //QVORTLIB_ERROR_H
