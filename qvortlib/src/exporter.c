#include <qvortlib/exporter.h>

#include <stdio.h>

#include "error.h"

void qv_save_geometry(const char* path, qv_geometry* geometry) {
    FILE* fp = fopen(path, "w");
    if (fp == NULL) SYSTEM_ERR("Failed to open file for writing");

    for (size_t i = 0; i < geometry->vertex_count; ++i) {
        qv_vec3* v = &geometry->vertex_buffer[i];
        fprintf(fp, "%f %f %f\n", v->x, v->y, v->z);
    }

    fclose(fp);
}
