#include <qvortlib/graph.h>

#include <assert.h>
#include <stdlib.h>

#include "error.h"

void qv_graph_init(qv_graph* graph, size_t size) {
    assert(size > 0);

    graph->size = size;
    graph->nodes = (qv_node*)malloc(size * sizeof(qv_node));
    if (graph->nodes == NULL) SYSTEM_ERR("Unable to allocate graph memory");
    for (size_t n = 0; n < size; ++n) {
        qv_node* node = &graph->nodes[n];
        node->in_edges_head = NULL;
        node->in_edges_tail = NULL;
        node->out_edges_head = NULL;
        node->out_edges_tail = NULL;
    }
}

static void free_edges(qv_edge **head, qv_edge **tail) {
    qv_edge* p = *head;
    while(p != NULL) {
        qv_edge* next = p->next;
        free(p);
        p = next;
    }
    *head = *tail = NULL;
}

void qv_graph_free(qv_graph* graph) {
    for (size_t n = 0; n < graph->size; n++) {
        qv_node* node = &graph->nodes[n];
        free_edges(&node->in_edges_head, &node->in_edges_tail);
        free_edges(&node->out_edges_head, &node->out_edges_tail);
    }
    graph->size = 0;
    free(graph->nodes);
    graph->nodes = NULL;
}

static qv_edge** find_edge(qv_edge** head, qv_edge** tail, size_t to) {
    qv_edge** p = head;
    while (*p != NULL) {
        if ((*p)->to == to)
            break;
        p = &(*p)->next;
    }
    return p;
}

static int add_edge(qv_edge** head, qv_edge** tail, size_t to) {
    qv_edge** p = find_edge(head, tail, to);
    if ((*p) == NULL) {
        *p = (qv_edge*) malloc(sizeof(qv_edge));
        if ((*p) == NULL) SYSTEM_ERR("Unable to allocate graph memory");
        (*p)->to = to;
        (*p)->next = NULL;
        return 1;
    } else {
        return 0;
    }
}

static int add_in_edge(qv_node* node, size_t to) {
    return add_edge(&node->in_edges_head, &node->in_edges_tail, to);
}

static int add_out_edge(qv_node* node, size_t to) {
    return add_edge(&node->out_edges_head, &node->out_edges_tail, to);
}

int qv_graph_add_edge(qv_graph *graph, size_t from, size_t to) {
    assert(graph != NULL);
    assert(from != to);
    assert(from < graph->size);
    assert(to < graph->size);

    qv_node* p_from = &graph->nodes[from];
    qv_node* p_to = &graph->nodes[to];

    add_out_edge(p_from, to);
    return add_in_edge(p_to, from);
}

int qv_graph_add_bi_edge(qv_graph *graph, size_t n1, size_t n2) {
    assert(graph != NULL);
    assert(n1 != n2);
    assert(n1 < graph->size);
    assert(n2 < graph->size);

    int e1 = qv_graph_add_edge(graph, n1, n2);
    int e2 = qv_graph_add_edge(graph, n2, n1);

    return e1 || e2;
}

const qv_edge *qv_graph_out_edges(const qv_graph *graph, size_t n) {
    assert(graph != NULL);
    assert(n < graph->size);

    return graph->nodes[n].out_edges_head;
}

const qv_edge *qv_graph_in_edges(const qv_graph *graph, size_t n) {
    assert(graph != NULL);
    assert(n < graph->size);

    return graph->nodes[n].in_edges_head;
}

size_t edge_count(qv_edge* edge) {
    size_t count = 0;
    while (edge != NULL) {
        edge = edge->next;
        count++;
    }
    return count;
}

size_t qv_graph_out_edge_count(qv_graph *graph, size_t n) {
    assert(graph != NULL);
    assert(n < graph->size);

    qv_node* node = &graph->nodes[n];
    return edge_count(node->out_edges_head);
}

size_t qv_graph_in_edge_count(qv_graph *graph, size_t n) {
    assert(graph != NULL);
    assert(n < graph->size);

    qv_node* node = &graph->nodes[n];
    return edge_count(node->in_edges_head);
}
