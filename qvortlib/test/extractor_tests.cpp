#include <gtest/gtest.h>

#include <qvortlib/extractor.h>

class ExtractorSingleCube : public ::testing::Test {
protected:

    void SetUp() {
        g = new qv_grid;
        g->x_size = 2;
        g->y_size = 2;
        g->z_size = 2;
        g->d_x = 1.0;
        g->d_y = 1.0;
        g->d_z = 1.0;
        g->deltas = new qv_complex[8];
    }

    void TearDown() {
        delete[] g->deltas;
        delete g;
    }

    qv_grid *g;
};

TEST_F(ExtractorSingleCube, XAxisVortex) {
    g->deltas[0] = {  0.0,  1.0 };
    g->deltas[1] = { -1.0,  0.0 };
    g->deltas[2] = {  1.0,  0.0 };
    g->deltas[3] = {  0.0, -1.0 };
    g->deltas[4] = {  0.0,  1.0 };
    g->deltas[5] = { -1.0,  0.0 };
    g->deltas[6] = {  1.0,  0.0 };
    g->deltas[7] = {  0.0, -1.0 };

    qv_vertices_store store;
    qv_vertices_store_init(&store, g);

    qv_generate_vertices(&store);

    ASSERT_EQ(store.geometry.vertex_count, 2);
    ASSERT_EQ(store.geometry.index_count, 2);

    EXPECT_EQ(store.geometry.vertex_buffer[0].x, 0.0);
    EXPECT_EQ(store.geometry.vertex_buffer[0].y, 0.5);
    EXPECT_EQ(store.geometry.vertex_buffer[0].z, 0.5);

    EXPECT_EQ(store.geometry.vertex_buffer[1].x, 1.0);
    EXPECT_EQ(store.geometry.vertex_buffer[1].y, 0.5);
    EXPECT_EQ(store.geometry.vertex_buffer[1].z, 0.5);

    EXPECT_EQ(store.geometry.index_buffer[0], 0);
    EXPECT_EQ(store.geometry.index_buffer[1], 1);

    qv_vertices_store_free(&store);
}
