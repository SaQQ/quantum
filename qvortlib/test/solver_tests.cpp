#include <gtest/gtest.h>

#include <qvortlib/solver.h>

class Vorticity : public ::testing::Test {
protected:
    double vorticity;
};

TEST_F(Vorticity, SingleEventRotation) {
    qv_complex points[] = {{-1.0, 0.0},
                           {0.0, -1.0},
                           {0.0, 1.0},
                           {1.0, 0.0}};
    vorticity = qv_get_vorticity(points);
    ASSERT_EQ(vorticity, 1.0);
}

TEST_F(Vorticity, SingleEven45Rotation) {
    qv_complex points[] = {{-1.0, -1.0},
                           {1.0, -1.0},
                           {-1.0, 1.0},
                           {1.0,  1.0}};
    vorticity = qv_get_vorticity(points);
    ASSERT_EQ(vorticity, 1.0);
}

TEST_F(Vorticity, DoubleEvenRotation) {
    qv_complex points[] = {{-1.0, -1.0},
                           {1.0, 1.0},
                           {1.0, 1.0},
                           {-1.0, -1.0}};
    vorticity = qv_get_vorticity(points);
    ASSERT_EQ(vorticity, 2.0);
}

class BilinearInterpolation : public ::testing::Test {
protected:
    double u, v;
    int result;
};

TEST_F(BilinearInterpolation, SingleEvenRotation) {
    qv_complex points[] = {{-1.0, 0.0},
                           {0.0, -1.0},
                           {0.0, 1.0},
                           {1.0, 0.0}};
    result = qv_solve_bilinear_interpolation(points, &u, &v);
    ASSERT_EQ(result, 0);
    EXPECT_EQ(u, 0.5);
    EXPECT_EQ(v, 0.5);
}

TEST_F(BilinearInterpolation, SingleEven45Rotation) {
    qv_complex points[] = {{-1.0, -1.0},
                           {1.0, -1.0},
                           {-1.0, 1.0},
                           {1.0,  1.0}};
    result = qv_solve_bilinear_interpolation(points, &u, &v);
    ASSERT_EQ(result, 0);
    EXPECT_EQ(u, 0.5);
    EXPECT_EQ(v, 0.5);
}
