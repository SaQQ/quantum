#include <gtest/gtest.h>

#include <qvortlib/graph.h>

class UndirectedGraphs : public ::testing::Test {
protected:

};

TEST_F(UndirectedGraphs, K2Segment) {
    qv_graph g;
    qv_graph_init(&g, 2);
    EXPECT_EQ(qv_graph_add_bi_edge(&g, 0, 1), 1);

    EXPECT_EQ(qv_graph_out_edge_count(&g, 0), 1);
    EXPECT_EQ(qv_graph_in_edge_count(&g, 0), 1);
    EXPECT_EQ(qv_graph_out_edge_count(&g, 1), 1);
    EXPECT_EQ(qv_graph_in_edge_count(&g, 1), 1);

    qv_graph_free(&g);
}

TEST_F(UndirectedGraphs, K3Cycle) {
    qv_graph g;
    qv_graph_init(&g, 3);

    EXPECT_EQ(qv_graph_add_bi_edge(&g, 0, 1), 1);
    EXPECT_EQ(qv_graph_add_bi_edge(&g, 1, 2), 1);
    EXPECT_EQ(qv_graph_add_bi_edge(&g, 2, 0), 1);

    EXPECT_EQ(qv_graph_out_edge_count(&g, 0), 2);
    EXPECT_EQ(qv_graph_in_edge_count(&g, 0), 2);
    EXPECT_EQ(qv_graph_out_edge_count(&g, 1), 2);
    EXPECT_EQ(qv_graph_in_edge_count(&g, 1), 2);
    EXPECT_EQ(qv_graph_out_edge_count(&g, 2), 2);
    EXPECT_EQ(qv_graph_in_edge_count(&g, 2), 2);

    qv_graph_free(&g);

}

class DirectedGraphs : public ::testing::Test {
protected:

};

TEST_F(DirectedGraphs, K2Segment) {
    qv_graph g;
    qv_graph_init(&g, 2);
    EXPECT_EQ(qv_graph_add_edge(&g, 0, 1), 1);

    EXPECT_EQ(qv_graph_out_edge_count(&g, 0), 1);
    EXPECT_EQ(qv_graph_in_edge_count(&g, 0), 0);
    EXPECT_EQ(qv_graph_out_edge_count(&g, 1), 0);
    EXPECT_EQ(qv_graph_in_edge_count(&g, 1), 1);

    qv_graph_free(&g);
}

TEST_F(DirectedGraphs, K3Cycle) {
    qv_graph g;
    qv_graph_init(&g, 3);

    EXPECT_EQ(qv_graph_add_edge(&g, 0, 1), 1);
    EXPECT_EQ(qv_graph_add_edge(&g, 1, 2), 1);
    EXPECT_EQ(qv_graph_add_edge(&g, 2, 0), 1);

    EXPECT_EQ(qv_graph_out_edge_count(&g, 0), 1);
    EXPECT_EQ(qv_graph_in_edge_count(&g, 0), 1);
    EXPECT_EQ(qv_graph_out_edge_count(&g, 1), 1);
    EXPECT_EQ(qv_graph_in_edge_count(&g, 1), 1);
    EXPECT_EQ(qv_graph_out_edge_count(&g, 2), 1);
    EXPECT_EQ(qv_graph_in_edge_count(&g, 2), 1);

    qv_graph_free(&g);

}

