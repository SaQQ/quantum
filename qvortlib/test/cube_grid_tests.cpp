#include <gtest/gtest.h>

#include <qvortlib/cube_grid.h>

class SingleCubeGrid : public ::testing::Test {
protected:

    void SetUp() override {
        g = new qv_grid;
        g->x_size = 2;
        g->y_size = 2;
        g->z_size = 2;
        g->d_x = 1.0;
        g->d_y = 2.0;
        g->d_z = 3.0;
        g->deltas = new qv_complex[8];
        g->deltas[0] = { 1.0, 1.0 };
        g->deltas[1] = { 2.0, 2.0 };
        g->deltas[2] = { 3.0, 3.0 };
        g->deltas[3] = { 4.0, 4.0 };
        g->deltas[4] = { 5.0, 5.0 };
        g->deltas[5] = { 6.0, 6.0 };
        g->deltas[6] = { 7.0, 7.0 };
        g->deltas[7] = { 8.0, 8.0 };
    }

    void TearDown() override {
        delete[] g->deltas;
        delete g;
    }

    qv_grid* g;
};

TEST_F(SingleCubeGrid, GetCube) {
    qv_cube cube;
    qv_get_cube(g, 0, 0, 0, &cube);
    EXPECT_EQ(cube.grid, g);
    EXPECT_EQ(cube.x, 0);
    EXPECT_EQ(cube.y, 0);
    EXPECT_EQ(cube.z, 0);
}

TEST_F(SingleCubeGrid, GetCubePoints) {
    qv_cube cube;
    qv_get_cube(g, 0, 0, 0, &cube);
    qv_vec3 point;
    qv_cube_get_point(&cube, 0, 0, 0, &point);
    EXPECT_EQ(point.x, 0.0); EXPECT_EQ(point.y, 0.0); EXPECT_EQ(point.z, 0.0);
    qv_cube_get_point(&cube, 0, 0, 1, &point);
    EXPECT_EQ(point.x, 0.0); EXPECT_EQ(point.y, 0.0); EXPECT_EQ(point.z, 3.0);
    qv_cube_get_point(&cube, 0, 1, 0, &point);
    EXPECT_EQ(point.x, 0.0); EXPECT_EQ(point.y, 2.0); EXPECT_EQ(point.z, 0.0);
    qv_cube_get_point(&cube, 0, 1, 1, &point);
    EXPECT_EQ(point.x, 0.0); EXPECT_EQ(point.y, 2.0); EXPECT_EQ(point.z, 3.0);
    qv_cube_get_point(&cube, 1, 0, 0, &point);
    EXPECT_EQ(point.x, 1.0); EXPECT_EQ(point.y, 0.0); EXPECT_EQ(point.z, 0.0);
    qv_cube_get_point(&cube, 1, 0, 1, &point);
    EXPECT_EQ(point.x, 1.0); EXPECT_EQ(point.y, 0.0); EXPECT_EQ(point.z, 3.0);
    qv_cube_get_point(&cube, 1, 1, 0, &point);
    EXPECT_EQ(point.x, 1.0); EXPECT_EQ(point.y, 2.0); EXPECT_EQ(point.z, 0.0);
    qv_cube_get_point(&cube, 1, 1, 1, &point);
    EXPECT_EQ(point.x, 1.0); EXPECT_EQ(point.y, 2.0); EXPECT_EQ(point.z, 3.0);
}

TEST_F(SingleCubeGrid, GetCubeInternalPoint) {
    qv_cube cube;
    qv_get_cube(g, 0, 0, 0, &cube);
    qv_vec3 point;
    qv_cube_get_internal_point(&cube, 0.5, 0.5, 0.5, &point);
    EXPECT_EQ(point.x, 0.5); EXPECT_EQ(point.y, 1.0); EXPECT_EQ(point.z, 1.5);
}

TEST_F(SingleCubeGrid, GetCubeDeltas) {
    qv_cube cube;
    qv_get_cube(g, 0, 0, 0, &cube);
    qv_complex delta;
    qv_cube_get_delta(&cube, 0, 0, 0, &delta);
    EXPECT_EQ(delta.real, 1.0); EXPECT_EQ(delta.imag, 1.0);
    qv_cube_get_delta(&cube, 0, 0, 1, &delta);
    EXPECT_EQ(delta.real, 2.0); EXPECT_EQ(delta.imag, 2.0);
    qv_cube_get_delta(&cube, 0, 1, 0, &delta);
    EXPECT_EQ(delta.real, 3.0); EXPECT_EQ(delta.imag, 3.0);
    qv_cube_get_delta(&cube, 0, 1, 1, &delta);
    EXPECT_EQ(delta.real, 4.0); EXPECT_EQ(delta.imag, 4.0);
    qv_cube_get_delta(&cube, 1, 0, 0, &delta);
    EXPECT_EQ(delta.real, 5.0); EXPECT_EQ(delta.imag, 5.0);
    qv_cube_get_delta(&cube, 1, 0, 1, &delta);
    EXPECT_EQ(delta.real, 6.0); EXPECT_EQ(delta.imag, 6.0);
    qv_cube_get_delta(&cube, 1, 1, 0, &delta);
    EXPECT_EQ(delta.real, 7.0); EXPECT_EQ(delta.imag, 7.0);
    qv_cube_get_delta(&cube, 1, 1, 1, &delta);
    EXPECT_EQ(delta.real, 8.0); EXPECT_EQ(delta.imag, 8.0);
}

TEST_F(SingleCubeGrid, GetXNegFace) {
    qv_face face;
    qv_get_face(g, 0, 0, 0, QV_FACE_X_NEG, &face);
    EXPECT_EQ(face.grid, g);
    EXPECT_EQ(face.type, QV_FACE_X_NEG);
    EXPECT_EQ(face.x, 0);
    EXPECT_EQ(face.y, 0);
    EXPECT_EQ(face.z, 0);
}

TEST_F(SingleCubeGrid, GetYNegFace) {
    qv_face face;
    qv_get_face(g, 0, 0, 0, QV_FACE_Y_NEG, &face);
    EXPECT_EQ(face.grid, g);
    EXPECT_EQ(face.type, QV_FACE_Y_NEG);
    EXPECT_EQ(face.x, 0);
    EXPECT_EQ(face.y, 0);
    EXPECT_EQ(face.z, 0);
}

TEST_F(SingleCubeGrid, GetZNegFace) {
    qv_face face;
    qv_get_face(g, 0, 0, 0, QV_FACE_Z_NEG, &face);
    EXPECT_EQ(face.grid, g);
    EXPECT_EQ(face.type, QV_FACE_Z_NEG);
    EXPECT_EQ(face.x, 0);
    EXPECT_EQ(face.y, 0);
    EXPECT_EQ(face.z, 0);
}

TEST_F(SingleCubeGrid, GetXPosFace) {
    qv_face face;
    qv_get_face(g, 0, 0, 0, QV_FACE_X_POS, &face);
    EXPECT_EQ(face.grid, g);
    EXPECT_EQ(face.type, QV_FACE_X_POS);
    EXPECT_EQ(face.x, 0);
    EXPECT_EQ(face.y, 0);
    EXPECT_EQ(face.z, 0);
}

TEST_F(SingleCubeGrid, GetYPosFace) {
    qv_face face;
    qv_get_face(g, 0, 0, 0, QV_FACE_Y_POS, &face);
    EXPECT_EQ(face.grid, g);
    EXPECT_EQ(face.type, QV_FACE_Y_POS);
    EXPECT_EQ(face.x, 0);
    EXPECT_EQ(face.y, 0);
    EXPECT_EQ(face.z, 0);
}

TEST_F(SingleCubeGrid, GetZPosFace) {
    qv_face face;
    qv_get_face(g, 0, 0, 0, QV_FACE_Z_POS, &face);
    EXPECT_EQ(face.grid, g);
    EXPECT_EQ(face.type, QV_FACE_Z_POS);
    EXPECT_EQ(face.x, 0);
    EXPECT_EQ(face.y, 0);
    EXPECT_EQ(face.z, 0);
}



TEST_F(SingleCubeGrid, GetXNegCubeFace) {
    qv_cube cube;
    qv_get_cube(g, 0, 0, 0, &cube);
    qv_face face;
    qv_get_cube_face(&cube, QV_FACE_X_NEG, &face);
    EXPECT_EQ(face.grid, g);
    EXPECT_EQ(face.type, QV_FACE_X_NEG);
    EXPECT_EQ(face.x, 0);
    EXPECT_EQ(face.y, 0);
    EXPECT_EQ(face.z, 0);
}

TEST_F(SingleCubeGrid, GetYNegCubeFace) {
    qv_cube cube;
    qv_get_cube(g, 0, 0, 0, &cube);
    qv_face face;
    qv_get_cube_face(&cube, QV_FACE_Y_NEG, &face);
    EXPECT_EQ(face.grid, g);
    EXPECT_EQ(face.type, QV_FACE_Y_NEG);
    EXPECT_EQ(face.x, 0);
    EXPECT_EQ(face.y, 0);
    EXPECT_EQ(face.z, 0);
}

TEST_F(SingleCubeGrid, GetZNegCubeFace) {
    qv_cube cube;
    qv_get_cube(g, 0, 0, 0, &cube);
    qv_face face;
    qv_get_cube_face(&cube, QV_FACE_Z_NEG, &face);
    EXPECT_EQ(face.grid, g);
    EXPECT_EQ(face.type, QV_FACE_Z_NEG);
    EXPECT_EQ(face.x, 0);
    EXPECT_EQ(face.y, 0);
    EXPECT_EQ(face.z, 0);
}

TEST_F(SingleCubeGrid, GetXPosCubeFace) {
    qv_cube cube;
    qv_get_cube(g, 0, 0, 0, &cube);
    qv_face face;
    qv_get_cube_face(&cube, QV_FACE_X_POS, &face);
    EXPECT_EQ(face.grid, g);
    EXPECT_EQ(face.type, QV_FACE_X_POS);
    EXPECT_EQ(face.x, 0);
    EXPECT_EQ(face.y, 0);
    EXPECT_EQ(face.z, 0);
}

TEST_F(SingleCubeGrid, GetYPosCubeFace) {
    qv_cube cube;
    qv_get_cube(g, 0, 0, 0, &cube);
    qv_face face;
    qv_get_cube_face(&cube, QV_FACE_Y_POS, &face);
    EXPECT_EQ(face.grid, g);
    EXPECT_EQ(face.type, QV_FACE_Y_POS);
    EXPECT_EQ(face.x, 0);
    EXPECT_EQ(face.y, 0);
    EXPECT_EQ(face.z, 0);
}

TEST_F(SingleCubeGrid, GetZPosCubeFace) {
    qv_cube cube;
    qv_get_cube(g, 0, 0, 0, &cube);
    qv_face face;
    qv_get_cube_face(&cube, QV_FACE_Z_POS, &face);
    EXPECT_EQ(face.grid, g);
    EXPECT_EQ(face.type, QV_FACE_Z_POS);
    EXPECT_EQ(face.x, 0);
    EXPECT_EQ(face.y, 0);
    EXPECT_EQ(face.z, 0);
}


TEST_F(SingleCubeGrid, GetXNegFacePoints) {
    qv_face face;
    qv_get_face(g, 0, 0, 0, QV_FACE_X_NEG, &face);
    qv_vec3 point;
    qv_face_get_point(&face, 0, 0, &point);
    EXPECT_EQ(point.x, 0.0); EXPECT_EQ(point.y, 0.0); EXPECT_EQ(point.z, 0.0);
    qv_face_get_point(&face, 0, 1, &point);
    EXPECT_EQ(point.x, 0.0); EXPECT_EQ(point.y, 0.0); EXPECT_EQ(point.z, 3.0);
    qv_face_get_point(&face, 1, 0, &point);
    EXPECT_EQ(point.x, 0.0); EXPECT_EQ(point.y, 2.0); EXPECT_EQ(point.z, 0.0);
    qv_face_get_point(&face, 1, 1, &point);
    EXPECT_EQ(point.x, 0.0); EXPECT_EQ(point.y, 2.0); EXPECT_EQ(point.z, 3.0);
}

TEST_F(SingleCubeGrid, GetYNegFacePoints) {
    qv_face face;
    qv_get_face(g, 0, 0, 0, QV_FACE_Y_NEG, &face);
    qv_vec3 point;
    qv_face_get_point(&face, 0, 0, &point);
    EXPECT_EQ(point.x, 0.0); EXPECT_EQ(point.y, 0.0); EXPECT_EQ(point.z, 0.0);
    qv_face_get_point(&face, 0, 1, &point);
    EXPECT_EQ(point.x, 1.0); EXPECT_EQ(point.y, 0.0); EXPECT_EQ(point.z, 0.0);
    qv_face_get_point(&face, 1, 0, &point);
    EXPECT_EQ(point.x, 0.0); EXPECT_EQ(point.y, 0.0); EXPECT_EQ(point.z, 3.0);
    qv_face_get_point(&face, 1, 1, &point);
    EXPECT_EQ(point.x, 1.0); EXPECT_EQ(point.y, 0.0); EXPECT_EQ(point.z, 3.0);
}

TEST_F(SingleCubeGrid, GetZNegFacePoints) {
    qv_face face;
    qv_get_face(g, 0, 0, 0, QV_FACE_Z_NEG, &face);
    qv_vec3 point;
    qv_face_get_point(&face, 0, 0, &point);
    EXPECT_EQ(point.x, 0.0); EXPECT_EQ(point.y, 0.0); EXPECT_EQ(point.z, 0.0);
    qv_face_get_point(&face, 0, 1, &point);
    EXPECT_EQ(point.x, 0.0); EXPECT_EQ(point.y, 2.0); EXPECT_EQ(point.z, 0.0);
    qv_face_get_point(&face, 1, 0, &point);
    EXPECT_EQ(point.x, 1.0); EXPECT_EQ(point.y, 0.0); EXPECT_EQ(point.z, 0.0);
    qv_face_get_point(&face, 1, 1, &point);
    EXPECT_EQ(point.x, 1.0); EXPECT_EQ(point.y, 2.0); EXPECT_EQ(point.z, 0.0);
}

TEST_F(SingleCubeGrid, GetXPosFacePoints) {
    qv_face face;
    qv_get_face(g, 0, 0, 0, QV_FACE_X_POS, &face);
    qv_vec3 point;
    qv_face_get_point(&face, 0, 0, &point);
    EXPECT_EQ(point.x, 1.0); EXPECT_EQ(point.y, 0.0); EXPECT_EQ(point.z, 0.0);
    qv_face_get_point(&face, 0, 1, &point);
    EXPECT_EQ(point.x, 1.0); EXPECT_EQ(point.y, 0.0); EXPECT_EQ(point.z, 3.0);
    qv_face_get_point(&face, 1, 0, &point);
    EXPECT_EQ(point.x, 1.0); EXPECT_EQ(point.y, 2.0); EXPECT_EQ(point.z, 0.0);
    qv_face_get_point(&face, 1, 1, &point);
    EXPECT_EQ(point.x, 1.0); EXPECT_EQ(point.y, 2.0); EXPECT_EQ(point.z, 3.0);
}

TEST_F(SingleCubeGrid, GetYPosFacePoints) {
    qv_face face;
    qv_get_face(g, 0, 0, 0, QV_FACE_Y_POS, &face);
    qv_vec3 point;
    qv_face_get_point(&face, 0, 0, &point);
    EXPECT_EQ(point.x, 0.0); EXPECT_EQ(point.y, 2.0); EXPECT_EQ(point.z, 0.0);
    qv_face_get_point(&face, 0, 1, &point);
    EXPECT_EQ(point.x, 1.0); EXPECT_EQ(point.y, 2.0); EXPECT_EQ(point.z, 0.0);
    qv_face_get_point(&face, 1, 0, &point);
    EXPECT_EQ(point.x, 0.0); EXPECT_EQ(point.y, 2.0); EXPECT_EQ(point.z, 3.0);
    qv_face_get_point(&face, 1, 1, &point);
    EXPECT_EQ(point.x, 1.0); EXPECT_EQ(point.y, 2.0); EXPECT_EQ(point.z, 3.0);
}

TEST_F(SingleCubeGrid, GetZPosFacePoints) {
    qv_face face;
    qv_get_face(g, 0, 0, 0, QV_FACE_Z_POS, &face);
    qv_vec3 point;
    qv_face_get_point(&face, 0, 0, &point);
    EXPECT_EQ(point.x, 0.0); EXPECT_EQ(point.y, 0.0); EXPECT_EQ(point.z, 3.0);
    qv_face_get_point(&face, 0, 1, &point);
    EXPECT_EQ(point.x, 0.0); EXPECT_EQ(point.y, 2.0); EXPECT_EQ(point.z, 3.0);
    qv_face_get_point(&face, 1, 0, &point);
    EXPECT_EQ(point.x, 1.0); EXPECT_EQ(point.y, 0.0); EXPECT_EQ(point.z, 3.0);
    qv_face_get_point(&face, 1, 1, &point);
    EXPECT_EQ(point.x, 1.0); EXPECT_EQ(point.y, 2.0); EXPECT_EQ(point.z, 3.0);
}


TEST_F(SingleCubeGrid, GetXNegFaceDeltas) {
    qv_face face;
    qv_get_face(g, 0, 0, 0, QV_FACE_X_NEG, &face);
    qv_complex delta;
    qv_face_get_delta(&face, 0, 0, &delta);
    EXPECT_EQ(delta.real, 1.0); EXPECT_EQ(delta.imag, 1.0);
    qv_face_get_delta(&face, 0, 1, &delta);
    EXPECT_EQ(delta.real, 2.0); EXPECT_EQ(delta.imag, 2.0);
    qv_face_get_delta(&face, 1, 0, &delta);
    EXPECT_EQ(delta.real, 3.0); EXPECT_EQ(delta.imag, 3.0);
    qv_face_get_delta(&face, 1, 1, &delta);
    EXPECT_EQ(delta.real, 4.0); EXPECT_EQ(delta.imag, 4.0);
}

TEST_F(SingleCubeGrid, GetYNegFaceDeltas) {
    qv_face face;
    qv_get_face(g, 0, 0, 0, QV_FACE_Y_NEG, &face);
    qv_complex delta;
    qv_face_get_delta(&face, 0, 0, &delta);
    EXPECT_EQ(delta.real, 1.0); EXPECT_EQ(delta.imag, 1.0);
    qv_face_get_delta(&face, 0, 1, &delta);
    EXPECT_EQ(delta.real, 5.0); EXPECT_EQ(delta.imag, 5.0);
    qv_face_get_delta(&face, 1, 0, &delta);
    EXPECT_EQ(delta.real, 2.0); EXPECT_EQ(delta.imag, 2.0);
    qv_face_get_delta(&face, 1, 1, &delta);
    EXPECT_EQ(delta.real, 6.0); EXPECT_EQ(delta.imag, 6.0);
}

TEST_F(SingleCubeGrid, GetZNegFaceDeltas) {
    qv_face face;
    qv_get_face(g, 0, 0, 0, QV_FACE_Z_NEG, &face);
    qv_complex delta;
    qv_face_get_delta(&face, 0, 0, &delta);
    EXPECT_EQ(delta.real, 1.0); EXPECT_EQ(delta.imag, 1.0);
    qv_face_get_delta(&face, 0, 1, &delta);
    EXPECT_EQ(delta.real, 3.0); EXPECT_EQ(delta.imag, 3.0);
    qv_face_get_delta(&face, 1, 0, &delta);
    EXPECT_EQ(delta.real, 5.0); EXPECT_EQ(delta.imag, 5.0);
    qv_face_get_delta(&face, 1, 1, &delta);
    EXPECT_EQ(delta.real, 7.0); EXPECT_EQ(delta.imag, 7.0);
}

TEST_F(SingleCubeGrid, GetXPosFaceDeltas) {
    qv_face face;
    qv_get_face(g, 0, 0, 0, QV_FACE_X_POS, &face);
    qv_complex delta;
    qv_face_get_delta(&face, 0, 0, &delta);
    EXPECT_EQ(delta.real, 5.0); EXPECT_EQ(delta.imag, 5.0);
    qv_face_get_delta(&face, 0, 1, &delta);
    EXPECT_EQ(delta.real, 6.0); EXPECT_EQ(delta.imag, 6.0);
    qv_face_get_delta(&face, 1, 0, &delta);
    EXPECT_EQ(delta.real, 7.0); EXPECT_EQ(delta.imag, 7.0);
    qv_face_get_delta(&face, 1, 1, &delta);
    EXPECT_EQ(delta.real, 8.0); EXPECT_EQ(delta.imag, 8.0);
}

TEST_F(SingleCubeGrid, GetYPosFaceDeltas) {
    qv_face face;
    qv_get_face(g, 0, 0, 0, QV_FACE_Y_POS, &face);
    qv_complex delta;
    qv_face_get_delta(&face, 0, 0, &delta);
    EXPECT_EQ(delta.real, 3.0); EXPECT_EQ(delta.imag, 3.0);
    qv_face_get_delta(&face, 0, 1, &delta);
    EXPECT_EQ(delta.real, 7.0); EXPECT_EQ(delta.imag, 7.0);
    qv_face_get_delta(&face, 1, 0, &delta);
    EXPECT_EQ(delta.real, 4.0); EXPECT_EQ(delta.imag, 4.0);
    qv_face_get_delta(&face, 1, 1, &delta);
    EXPECT_EQ(delta.real, 8.0); EXPECT_EQ(delta.imag, 8.0);
}

TEST_F(SingleCubeGrid, GetZPosFaceDeltas) {
    qv_face face;
    qv_get_face(g, 0, 0, 0, QV_FACE_Z_POS, &face);
    qv_complex delta;
    qv_face_get_delta(&face, 0, 0, &delta);
    EXPECT_EQ(delta.real, 2.0); EXPECT_EQ(delta.imag, 2.0);
    qv_face_get_delta(&face, 0, 1, &delta);
    EXPECT_EQ(delta.real, 4.0); EXPECT_EQ(delta.imag, 4.0);
    qv_face_get_delta(&face, 1, 0, &delta);
    EXPECT_EQ(delta.real, 6.0); EXPECT_EQ(delta.imag, 6.0);
    qv_face_get_delta(&face, 1, 1, &delta);
    EXPECT_EQ(delta.real, 8.0); EXPECT_EQ(delta.imag, 8.0);
}
