#ifndef QVORTLIB_GRID_H
#define QVORTLIB_GRID_H

/** @file Complex field type and functions. */

#include <stddef.h>

#include "maths.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 3D complex field.
 * Represents 3D discrete field of complex values.
 */
typedef struct qv_grid_t {
    /** @brief Number of samples along x-axis. */
    size_t x_size;
    /** @brief Number of samples along y-axis. */
    size_t y_size;
    /** @brief Number of samples along z-axis. */
    size_t z_size;
    /**
     * @brief Resolution of samples along x-axis.
     * This is distance between two consecutive samples.
     */
    double d_x;
    /**
     * @brief Resolution of samples along y-axis.
     * This is distance between two consecutive samples.
     */
    double d_y;
    /**
     * @brief Resolution of samples along z-axis.
     * This is distance between two consecutive samples.
     */
    double d_z;
    /** @brief Complex data array. */
    qv_complex* deltas;
} qv_grid;

/**
 * @brief Initialization of qv_grid instance.
 * Constructs qv_grid instance, allocating storage buffers and setting all member variables of instance.
 * @param grid Grid that is to be initialized.
 * @param x Field x_size.
 * @param y Field y_size.
 * @param z Field z_size.
 * @param d_x Field x-resolution.
 * @param d_y Field y-resolution.
 * @param d_z Field z-resolution.
 */
void qv_grid_init(qv_grid *grid, size_t x, size_t y, size_t z, double d_x, double d_y, double d_z);

/**
 * @brief Destruction of qv_grid instance.
 * De-initializes grid, freeing all allocated memory, and defaulting member variables.
 * @param grid Grid that is to be destructed.
 * @remark After call to this function parameter grid is no longer usable.
 */
void qv_grid_free(qv_grid* grid);

/**
 * @brief Complex field value getter.
 * @param grid Initialized grid instance.
 * @param x Sample's x index.
 * @param y Sample's y index.
 * @param z Sample's z index.
 * @param delta Pointer to complex number where field value will be stored.
 */
void qv_grid_get_delta(const qv_grid* grid, size_t x, size_t y, size_t z, qv_complex* delta);

/**
 * @brief Complex field value setter.
 * @param grid Initialized grid instance.
 * @param x Sample's x index.
 * @param y Sample's y index.
 * @param z Sample's z index.
 * @param value Complex number that will be copied to the grid's buffer.
 */
void gv_grid_set_delta(qv_grid* grid, size_t x, size_t y, size_t z, qv_complex* value);

/**
 * @brief Position of sample.
 * Calculates position of complex sample in field using it's indices and grid's resolution.
 * Sample with indices (0, 0, 0) has always coordinates (0.0, 0.0, 0.0).
 * @param grid Initialized grid instance.
 * @param x Sample's x index.
 * @param y Sample's y index.
 * @param z Sample's z index.
 * @param point Pointer to 3D vector where calculated position will be stored.
 */
void qv_grid_get_point(const qv_grid* grid, size_t x, size_t y, size_t z, qv_vec3* point);

#ifdef __cplusplus
}
#endif

#endif //QVORTLIB_GRID_H
