#ifndef QVORTLIB_GRAPH_ALGO_H
#define QVORTLIB_GRAPH_ALGO_H

/** @file Graph algorithms. */

#include "graph.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Connected components evaluation.
 * Algorithm traverses graph depth-first identifying connected component index for each node.
 * Disconnected nodes receive different component indices.
 * @param graph Graph that is to be analyzed.
 * @param component Pre-allocated array of component indices that will be filled by the algorithm.
 * Array size shall be at least graph->size. As a result component[i] contains index of connected component of i'th
 * graph node. Components are indexed from 0.
 * @return Number of distinct connected components.
 */
size_t qv_connected_components(const qv_graph* graph, size_t* component);

#ifdef __cplusplus
}
#endif

#endif //QVORTLIB_GRAPH_ALGO_H
