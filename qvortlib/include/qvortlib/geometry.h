#ifndef QVORTLIB_GEOMETRY_H
#define QVORTLIB_GEOMETRY_H

/** @file Vortex core geometry structure and functions. */

#include "grid.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Vortex core geometry.
 * Stores vortex core geometry in a graphics structures (vertex/index buffer).
 */
typedef struct qv_geometry_t {
    /** @brief Number of vertices in a buffer. */
    size_t vertex_count;
    /** @brief Dynamically allocated buffer of vertex positions. */
    qv_vec3* vertex_buffer;
    /** @brief Size of buffer pointed by vertex_buffer (in vertices, not in bytes). */
    size_t vertex_buffer_size;

    /** @brief Number of indices in a buffer. */
    size_t index_count;
    /** @brief Dynamically allocated buffer of vertex indices. */
    size_t* index_buffer;
    /** @brief Size of buffer pointer by index_buffer (in indices, not in bytes). */
    size_t index_buffer_size;
} qv_geometry;

/**
 * @brief Initialization of qv_geometry instance.
 * Constructs qv_grid instance, allocating minimum-size storage buffers and setting all member variables of instance.
 * @param geometry Geometry that is to be initialized.
 */
void qv_geometry_init(qv_geometry* geometry);

/**
 * @brief Destruction of qv_geometry instance.
 * De-initializes geometry, freeing all allocated memory, and defaulting member variables.
 * @param geometry Geometry that is to be destructed.
 */
void qv_geometry_free(qv_geometry* geometry);

/**
 * @brief Append vertex to the geometry.
 * Appends single vertex to the vertex buffer possibly increasing it's size and as a consequence, reallocating it.
 * @param geometry Geometry that received additional vertex.
 * @param vertex Vertex to be appended.
 * @return Index of a newly appended vertex to be used in the index buffer.
 */
size_t qv_geometry_append_vertex(qv_geometry* geometry, const qv_vec3* vertex);

/**
 * @brief Append index to the geometry.
 * Appends single index to the index buffer possibly increasing it's size and as a consequence, reallocating it.
 * @param geometry Geometry that received additional index.
 * @param idx Index to be appended.
 * @remark As qv_geometry represents set of 3D line segments single index appended does not change graphical
 * representation. Two new indices are required to define additional segment.
 */
void qv_geometry_append_index(qv_geometry* geometry, size_t idx);

#ifdef __cplusplus
}
#endif

#endif //QVORTLIB_GEOMETRY_H
