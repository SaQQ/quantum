#ifndef QVORTLIB_GRAPH_H
#define QVORTLIB_GRAPH_H

/** @file Basic graph structure and operations. */

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Graph edge.
 * Structure used in implementation of single node's edge linked list.
 */
typedef struct qv_edge_t {
    /** @brief Index of node that the edge leads to. */
    size_t to;
    /** @brief Pointer to the next edge in list. */
    struct qv_edge_t* next;
} qv_edge;

/**
 * @brief Graph node.
 */
typedef struct qv_node_t {
    /** @brief Head of incoming edge list. */
    qv_edge* in_edges_head;
    /** @brief Tail of incoming edge list. */
    qv_edge* in_edges_tail;
    /** @brief Head of outgoing edge list. */
    qv_edge* out_edges_head;
    /** @brief Tail of outgoing edge list. */
    qv_edge* out_edges_tail;
} qv_node;

/**
 * @brief Graph.
 * Implements graph structure as an adjacency list. Nodes count is immutable and set during initialization.
 * Can represent both directed and undirected simple graphs (multiple overlapping edges are not allowed).
 */
typedef struct qv_graph_t {
    /** @brief Number of graph nodes. */
    size_t size;
    /** @brief Dynamically allocated node's array. */
    qv_node* nodes;
} qv_graph;

/**
 * @brief Initialization of qv_graph instance.
 * Constructs qv_graph instance, allocating node's storage buffers and setting all member variables of instance.
 * After construction, graph has no edges.
 * @param graph Graph that is to be initialized.
 * @param n Number of graph nodes.
 */
void qv_graph_init(qv_graph* graph, size_t n);

/**
 * @brief Destruction of qv_graph instance.
 * De-initializes graph, freeing all allocated memory, and defaulting member variables.
 * @param graph Graph that is to be destructed.
 */
void qv_graph_free(qv_graph* graph);

/**
 * @brief Adds directed edge to the graph.
 * @param graph Graph that receives new edge.
 * @param from Edge's outgoing node index.
 * @param to Edge's incoming node index.
 * @return 0 if the edge existed before, 1 otherwise.
 */
int qv_graph_add_edge(qv_graph *graph, size_t from, size_t to);

/**
 * @brief Add undirected (bi-directional) edge to the graph.
 * @param graph Graph that receives new edge.
 * @param n1 Edge's first end node index.
 * @param n2 Edge's second end node index.
 * @return 0 if the edge existed before, 1 otherwise.
 */
int qv_graph_add_bi_edge(qv_graph *graph, size_t n1, size_t n2);

/**
 * @brief Outgoing edge list accessor.
 * @param graph Graph.
 * @param n Node index for which edge list is requested.
 * @return Head of outgoing edge list (possibly null).
 */
const qv_edge* qv_graph_out_edges(const qv_graph* graph, size_t n);

/**
 * @brief Incoming edge list accessor.
 * @param graph Graph.
 * @param n Node index for which edge list is requested.
 * @return Head of incoming edge list (possibly null).
 */
const qv_edge* qv_graph_in_edges(const qv_graph* graph, size_t n);

/**
 * @brief Outgoing edges counter.
 * @param graph Graph.
 * @param n Node index for which edges will be counted.
 * @return Number of edges going out from node n.
 * @remark This function fully traverses edge list leading to O(n) complexity.
 */
size_t qv_graph_out_edge_count(qv_graph *graph, size_t n);

/**
 * @brief Incoming edges counter.
 * @param graph Graph.
 * @param n Node index for which edges will be counted.
 * @return Number of edges coming in node n.
 * @remark This function fully traverses edge list leading to O(n) complexity.
 */
size_t qv_graph_in_edge_count(qv_graph *graph, size_t n);

#ifdef __cplusplus
}
#endif

#endif //QVORTLIB_GRAPH_H
