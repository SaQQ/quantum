#ifndef QVORTLIB_STATISTICS_H
#define QVORTLIB_STATISTICS_H

/** @file Calculation of statistical features of extracted vortex core. */

#include "qvortlib/geometry.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Decomposition of vortex core into multiple connected components.
 * As main algorithm generates single geometry of all vortices that are present in grid
 * this function allows for separation of independent vortices based on graph connectivity.
 * @param geometry Geometry of source vortex that is possibly not connected.
 * @return Number of connected components.
 * @todo This function should return each component's geometry separately.
 */
size_t qv_split_vortices(const qv_geometry* geometry);

/**
 * @brief Calculation of total length of vortex curves.
 * @param geometry Vortex core geometry (possibly not connected).
 * @return Total length of vortex curves.
 */
double qv_vortex_length(const qv_geometry* geometry);

#ifdef __cplusplus
}
#endif

#endif //QVORTLIB_STATISTICS_H
