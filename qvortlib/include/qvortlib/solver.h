#ifndef QVORTLIB_SOLVER_H
#define QVORTLIB_SOLVER_H

/** @file Auxiliary functions used by vortex extraction algorithm. */

#include "grid.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Detection of discrete field rotation along closed rectangular path.
 * Algorithm takes complex field values at four corners of a rectangle and checks if the arguments of complex numbers
 * are steadily increasing/decreasing along the path. It calculates the number of full turns and returns it as a
 * number called 'vorticity'. Non-zero vorticity means that there is a vortex core point somewhere inside the rectangle.
 * Positive vorticity represents clockwise rotation.
 * @param deltas 4 complex field values in corners of checked rectangle.
 * Order is the following: lower-left, lower-right, upper-left, upper-right.
 * @return Calculated vorticity. 0 if no rotation occurs or algorithm could not detect one.
 */
int qv_get_vorticity(qv_complex deltas[4]);

/**
 * @brief Find fine location of vortex core on rectangular face by interpolating corner values.
 * Algorithm finds root of bilinear interpolation of complex values in corners of the rectangle and stores
 * parametrization coordinates of this root in memory pointed by parameters (u, v). If unsolvable degeneration of occurs
 * or root is out of the domain [0, 1] x [0, 1] function reports failure.
 * @param deltas deltas 4 complex field values in corners of checked rectangle.
 * Order is the following: lower-left, lower-right, upper-left, upper-right.
 * Parametrization is constructed as (1-u)(1-v)deltas[0] + (u)(1-v)deltas[1] + (1-u)(v)deltas[2] + (u)(v)deltas[3].
 * @param u Storage for the first parameter of found root.
 * @param v Storage for the second parameter of found root.
 * @return 0 in case of success, -1 otherwise.
 * @pre qv_get_vorticity(deltas) != 0
 */
int qv_solve_bilinear_interpolation(qv_complex deltas[4], double* u, double* v);

#ifdef __cplusplus
}
#endif

#endif //QVORTLIB_SOLVER_H
