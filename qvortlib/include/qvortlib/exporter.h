#ifndef QVORTLIB_EXPORTER_H
#define QVORTLIB_EXPORTER_H

/** @file Algorithm output exporting functions. */

#include "geometry.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Export vortex core geometry to file in textual format.
 * Saves vortex core points line-by-line. Each line contains space-separated point coordinates X Y Z.
 * @param path Filesystem target file path (it will be overwritten if existing).
 * @param geometry Vortex core geometry to be exported.
 */
void qv_save_geometry(const char* path, qv_geometry* geometry);

#ifdef __cplusplus
}
#endif

#endif //QVORTLIB_EXPORTER_H
