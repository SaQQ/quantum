#ifndef QVORTLIB_CUBE_GRID_H
#define QVORTLIB_CUBE_GRID_H

/** @file Helper functions for treating discrete complex field as array of cubes. */

#include "grid.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Type of cube's face.
 */
typedef enum qv_face_type_e {
    QV_FACE_X_NEG = 0,
    QV_FACE_X_POS = 1,
    QV_FACE_Y_NEG = 2,
    QV_FACE_Y_POS = 3,
    QV_FACE_Z_NEG = 4,
    QV_FACE_Z_POS = 5
} qv_face_type;

/**
 * @brief Grid cube spanned on 8 field samples
 * Cube is described by (x, y, z) position in discrete field. It is spanned on samples
 * with indices (x (+1), y (+1), z (+1)). Accessor functions use x_pos, y_pos, z_pos parameters for controlling
 * whether add +1 to the sample index or not what allows for querying all 8 field values.
 * Cube's volume is naturally parametrized by (x, y, z) parameters.
 */
typedef struct qv_cube_t {
    /** @brief Grid on which cube is spanned */
    const qv_grid* grid;
    /** @brief X index of cube */
    size_t x;
    /** @brief Y index of cube */
    size_t y;
    /** @brief Z index of cube */
    size_t z;
} qv_cube;

/**
 * @brief Constructs qv_cube instance from location in complex grid.
 * @param grid Field on which cube will be spanned.
 * @param x X index of cube.
 * @param y Y index of cube.
 * @param z Z index of cube.
 * @param cube Pointer to qv_cube instance which will be initialized.
 */
void qv_get_cube(const qv_grid* grid, size_t x, size_t y, size_t z, qv_cube* cube);

/**
 * @brief Cube's complex value accessor.
 * This function can return complex field value in one of 8 cube's vertices.
 * @param cube Initialized qv_cube instance.
 * @param x_pos Binary flag picking negative X face vertex (if 0) or positive X face vertex (if 1).
 * @param y_pos Binary flag picking negative Y face vertex (if 0) or positive Y face vertex (if 1).
 * @param z_pos Binary flag picking negative Z face vertex (if 0) or positive Z face vertex (if 1).
 * @param delta Pointer to complex number where sampled field value will be stored.
 */
void qv_cube_get_delta(const qv_cube* cube, int x_pos, int y_pos, int z_pos, qv_complex* delta);

/**
 * @brief Cube's sample position.
 * @param cube Initialized qv_cube instance.
 * @param x_pos Binary flag picking negative X face vertex (if 0) or positive X face vertex (if 1).
 * @param y_pos Binary flag picking negative Y face vertex (if 0) or positive Y face vertex (if 1).
 * @param z_pos Binary flag picking negative Z face vertex (if 0) or positive Z face vertex (if 1).
 * @param point Pointer to 3D vector where calculated sample's position will be stored.
 */
void qv_cube_get_point(const qv_cube* cube, int x_pos, int y_pos, int z_pos, qv_vec3* point);

/**
 * @brief Interpolated position in cube's volume.
 * @param cube Initialized qv_cube instance.
 * @param x First parameter of trilinear interpolation.
 * @param y Second parameter of trilinear interpolation.
 * @param z Third parameter of trilinear interpolation.
 * @param point Pointer to 3D vector where calculated position will be stored.
 */
void qv_cube_get_internal_point(const qv_cube* cube, double x, double y, double z, qv_vec3* point);

/**
 * @brief Cube's face spanned on 4 field samples.
 * Face is described (not uniquely) by cube's position and face type
 * (face can be related to two cubes on its both sides). Face is naturally parametrized by (u, v) parameters
 * which exact meaning depends on the face type.
 */
typedef struct qv_face_t {
    /** @brief Grid on which face/cube is spanned */
    const qv_grid* grid;
    /** @brief X index of cube */
    size_t x;
    /** @brief Y index of cube */
    size_t y;
    /** @brief Z index of cube */
    size_t z;
    /** @brief Type of cube's face (one of 6 possible) */
    qv_face_type type;
} qv_face;

/**
 * @brief Constructs qv_face instance directly from location of cube in complex grid.
 * @param grid Field on which face will be spanned.
 * @param x X position of cube.
 * @param y Y position of cube.
 * @param z Z position of cube.
 * @param type One of 6 possible cube's faces.
 * @param face Pointer to qv_face instance which will be initialized.
 */
void qv_get_face(const qv_grid* grid, size_t x, size_t y, size_t z, qv_face_type type, qv_face* face);

/**
 * @brief Constructs qv_face instance as one of qv_cube faces.
 * @param cube Cube from which face will be derived.
 * @param type One of 6 possible cube's faces.
 * @param face Pointer to qv_face instance which will be initialized.
 */
void qv_get_cube_face(const qv_cube* cube, qv_face_type type, qv_face* face);

/**
 * @brief Face's complex field value accessor.
 * Function queries sample in one of face corners characterized by (u_pos, v_pos) parameters.
 * @param face Initialized qv_face instance.
 * @param u_pos Binary flag.
 * @param v_pos Binary flag.
 * @param delta Pointer to a complex number where field value will be stored.
 */
void qv_face_get_delta(const qv_face* face, int u_pos, int v_pos, qv_complex* delta);

/**
 * @brief Face's sample position.
 * Function queries sample in one of face corners characterized by (u_pos, v_pos) parameters.
 * @param face Initialized qv_face instance.
 * @param u_pos Binary flag.
 * @param v_pos Binary flag.
 * @param point Pointer to 3D vector where calculated position will be stored.
 */
void qv_face_get_point(const qv_face* face, int u_pos, int v_pos, qv_vec3* point);

/**
 * @brief Interpolated position on face's surface.
 * @param face Initialized qv_face instance.
 * @param u First bilinear parametrization parameter.
 * @param v Second bilinear parametrization parameter.
 * @param point Pointer to 3D vector where interpolated position will be stored.
 */
void qv_face_get_internal_point(const qv_face* face, double u, double v, qv_vec3* point);

#ifdef __cplusplus
}
#endif

#endif //QVORTLIB_CUBE_GRID_H
