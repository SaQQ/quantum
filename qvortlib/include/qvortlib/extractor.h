#ifndef QVORTLIB_EXTRACTOR_H
#define QVORTLIB_EXTRACTOR_H

/** @file Vortex extraction functions. */

#include "vertices_store.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Vortex extraction algorithm.
 * Algorithm analyzes complex assigned to provided vertices store and populates the store with generated vortex core
 * vertices and indices. As a result store->geometry contains full geometry of vortex core (possibly not-connected).
 * @param store Initialized, unpopulated vertices store.
 */
void qv_generate_vertices(qv_vertices_store* store);

#ifdef __cplusplus
}
#endif

#endif //QVORTLIB_EXTRACTOR_H
