#ifndef QVORTLIB_GRID_LOADER_H
#define QVORTLIB_GRID_LOADER_H

/** @file Complex field loading functions. */

#include <qvortlib/grid.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
* @brief Complex field loader (binary format).
* Reads binary file of custom format containing complex field data and initializes instance of qv_grid.
* As a binary file can contain multiple frames an index has to be provided. Other frames are skipped.
* @param path Filesystem path to the file.
* @param grid Grid to be initialized.
* @param frame_index Index of frame to be loaded.
*/
void qv_read_binary_file(const char* path, qv_grid* grid, size_t frame_index);

/**
 * @brief Complex field loader (textual format).
 * @param path Filesystem path to the file.
 * @param grid Grid to be initialized.
 * @todo This function is not yet implemented.
 * @warning This function is not yet implemented.
 */
void qv_read_text_file(const char* path, qv_grid* grid);

#ifdef __cplusplus
}
#endif

#endif //QVORTLIB_GRID_LOADER_H
