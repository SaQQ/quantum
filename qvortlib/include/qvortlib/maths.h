#ifndef QVORTLIB_VECTOR_H
#define QVORTLIB_VECTOR_H

/** @file Mathematical structures and operators. */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 3D vector type.
 * Used in representation of grid and vortices geometry.
 */
typedef struct qv_vec3_t {
    double x;
    double y;
    double z;
} qv_vec3;

/**
 * @brief 3D vector addition.
 * Adds two 3D vectors as in expression *lhs += *rhs.
 * @param lhs Left hand side of + operand.
 * @param rhs Right hand side of + operand.
 */
void qv_vec3_add(qv_vec3* lhs, const qv_vec3* rhs);

/**
 * @brief 3D vector subtraction.
 * Subtracts two 3D vectors as in expression *lhs -= *rhs.
 * @param lhs Left hand side of - operand.
 * @param rhs Right hand side of - operand.
 */
void qv_vec3_sub(qv_vec3* lhs, const qv_vec3* rhs);

/**
 * @brief 3D vector multiplication.
 * Multiplies 3D vectors by scalar value (component-wise) as in expression *lhs *= *rhs.
 * @param lhs Left hand size of * operand.
 * @param rhs Right hand size of * operand (scalar value).
 */
void qv_vec3_mul(qv_vec3* lhs, double rhs);

/**
 * @brief 3D vector division.
 * Divides 3D vectors by scalar value (component-wise) as in expression *lhs /= *rhs.
 * @param lhs Left hand size of / operand.
 * @param rhs Right hand size of / operand (scalar value).
 */
void qv_vec3_div(qv_vec3* lhs, double rhs);

/**
 * @brief Magnitude of 3D vector.
 * @param vec Vector for which magnitude is to be calculated.
 * @return Calculated magnitude.
 */
double qv_vec3_length(const qv_vec3* vec);

/**
 * @brief Squared magnitude of 3D vector.
 * This function is faster than qv_vec3_length() as it does not calculate square root.
 * @param vec Vector for which magnitude is to be calculated.
 * @return Calculated squared magnitude.
 */
double qv_vec3_length_sq(const qv_vec3* vec);

/**
 * @brief Complex number type.
 * Used in representation of values of complex field.
 */
typedef struct qv_complex_t {
    double real;
    double imag;
} qv_complex;

/**
 * @brief Argument of complex number.
 * @param c Complex number for which argument is to be calculated.
 * @return Calculated argument.
 */
double qv_complex_arg(const qv_complex* c);

/**
 * @brief Magnitude of complex number.
 * @param c Complex number for which magnitude is to be calculated.
 * @return Calculated magnitude.
 */
double qv_complex_mag(const qv_complex* c);

/**
 * @brief Squared magnitude of complex number.
 * This function is faster than qv_complex_mag() as it does not calculate square root.
 * @param c Complex number for which magnitude is to be calculated.
 * @return Calculated magnitude.
 */
double qv_complex_mag_sq(const qv_complex* c);

/**
 * @brief Addition of complex numbers.
 * Adds two complex numbers as in expression *lhs += *rhs.
 * @param lhs Left hand size of + operand.
 * @param rhs Right hand size of + operand.
 */
void qv_complex_add(qv_complex* lhs, const qv_complex* rhs);

/**
 * @brief Subtraction of complex numbers.
 * Subtracts two complex numbers as in expression *lhs -= *rhs.
 * @param lhs Left hand size of - operand.
 * @param rhs Right hand size of * operand.
 */
void qv_complex_sub(qv_complex* lhs, const qv_complex* rhs);

/**
 * @brief Multiplication of complex number.
 * Multiplies complex value by scalar as in expression *lhs *= *rhs.
 * @param lhs Left hand size of * operand.
 * @param rhs Right hand size of * operand (scalar value).
 */
void qv_complex_mul(qv_complex* lhs, double rhs);

/**
 * @brief Division of complex numbers.
 * Divides complex value by scalar as in expression *lsh /= *rhs.
 * @param lhs Left hand size of / operand.
 * @param rhs Right hand size of / operand (scalar value).
 */
void qv_complex_div(qv_complex* lhs, double rhs);

#ifdef __cplusplus
}
#endif

#endif
