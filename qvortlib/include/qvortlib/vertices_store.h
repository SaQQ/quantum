#ifndef QVORTLIB_VERTICES_STORE_H
#define QVORTLIB_VERTICES_STORE_H

/** @brief Vortex extraction algorithm main data structure - vertices store structure and operations. */

#include "qvortlib/cube_grid.h"
#include "qvortlib/geometry.h"

#ifdef __cplusplus
extern "C"{
#endif

/**
 * @brief Optional vertex index stored in vertices store.
 * This is optional integer type that should be processes using macros.
 * @see QV_IDX_HAS_VALUE
 * @see QV_IDX_VALUE
 * @see QV_IDX_MAKE_VALUE
 */
typedef size_t qv_idx_opt_t;

/** @brief Boolean predicate determining if argument of type qv_idx_opt_t has value. */
#define QV_IDX_HAS_VALUE(idx) ((idx) > 1u)
/**
 * @brief Value extraction macro for type qv_idx_opt_t .
 * @pre QV_IDX_HAS_VALUE(idx)
 */
#define QV_IDX_VALUE(idx) ((idx) - 1u)
/**
 * @brief Creation of value of type qv_idx_opt_t.
 * @post QV_IDX_HAS_VALUE(idx)
 */
#define QV_IDX_MAKE_VALUE(idx) ((idx) + 1u)

/**
 * @brief Vortex vertices store for complex field.
 * Container for vertices of vortex core extracted from complex field. Discrete field is treated as grid of
 * cubes of size (grid->x_size - 1, grid->y_size - 1, grid->z_size - 1). Each cube can have a vertex generated on each
 * of 6 faces. There can also be a vertex generated inside a cube. Vertices store is a container for this kind of
 * structure. This is main data structure used by vortex extraction algorithm.
 */
typedef struct qv_vertices_store_t {
    /** @brief Complex field for which vertices store was created. */
    const qv_grid* grid;

    /** @brief Optional indices of vertices generated inside the cubes. */
    qv_idx_opt_t* internal_indices;
    /** @brief Optional indices of vertices generated on faces perpendicular to x-axis. */
    qv_idx_opt_t* x_face_indices;
    /** @brief Optional indices of vertices generated on faces perpendicular to y-axis. */
    qv_idx_opt_t* y_face_indices;
    /** @brief Optional indices of vertices generated on faces perpendicular to z-axis. */
    qv_idx_opt_t* z_face_indices;

    /** @brief Vortex geometry containing vertices pointed by optional indices in store. */
    qv_geometry geometry;
} qv_vertices_store;

/**
 * @brief Constructs qv_vertices store instance from complex field.
 * Constructs qv_vertices_store instance, allocating storage buffers and setting all member variables of instance.
 * @param store Vertices store that is to be initialized.
 * @param grid Pointer to grid associated with vertices store.
 * @remark Grid's lifespan has to be longer than initialized store instance.
 */
void qv_vertices_store_init(qv_vertices_store* store, const qv_grid* grid);

/**
 * @brief Destruction of qv_vertices_store instance.
 * De-initializes store, freeing all allocated memory, and defaulting member variables.
 * @param store Vertices store that is to be destructed.
 */
void qv_vertices_store_free(qv_vertices_store* store);

/**
 * @brief X-size of vertices store.
 * @param store Initialized qv_vertices_store instance.
 * @return Number of cubes along X axis (grid->x_size - 1).
 */
size_t qv_vertices_store_x_size(const qv_vertices_store* store);

/**
 * @brief Y-size of vertices store.
 * @param store Initialized qv_vertices_store instance.
 * @return Number of cubes along Y axis (grid->y_size - 1).
 */
size_t qv_vertices_store_y_size(const qv_vertices_store* store);

/**
 * @brief Z-size of vertices store.
 * @param store Initialized qv_vertices_store instance.
 * @return Number of cubes along Z axis (grid->z_size - 1).
 */
size_t qv_vertices_store_z_size(const qv_vertices_store* store);

/**
 * @brief Maximum number of vertices in store.
 * @param store Initialized qv_vertices_store instance.
 * @return Calculated maximum number of vertices in store (on all faces, and internal ones).
 */
size_t qv_vertices_store_capacity(const qv_vertices_store* store);

/**
 * @brief Internal cube's vertex setter.
 * Appends vertex to store's geometry and saves its index in store's indices.
 * If vertex already exists it will be overwritten.
 * @param store Initialized qv_vertices_store instance.
 * @param x X index of cube.
 * @param y Y index of cube.
 * @param z Z index of cube.
 * @param vertex Vertex position to be set.
 * @return Index of new vertex in geometry.
 */
size_t qv_vertices_store_set_internal_vertex(qv_vertices_store* store, size_t x, size_t y, size_t z,
                                             const qv_vec3* vertex);

/**
 * @brief X face vertex setter.
 * Appends vertex to store's geometry and saves its index in store's indices.
 * If vertex already exists it will be overwritten.
 * @param store Initialized qv_vertices_store instance.
 * @param x X index of face.
 * @param y Y index of face.
 * @param z Z index of face.
 * @param vertex Vertex position to be set.
 * @return Index of new vertex in geometry.
 */
size_t qv_vertices_store_set_x_face_vertex(qv_vertices_store* store, size_t x, size_t y, size_t z,
                                           const qv_vec3* vertex);

/**
 * @brief Y face vertex setter.
 * Appends vertex to store's geometry and saves its index in store's indices.
 * If vertex already exists it will be overwritten.
 * @param store Initialized qv_vertices_store instance.
 * @param x X index of face.
 * @param y Y index of face.
 * @param z Z index of face.
 * @param vertex Vertex position to be set.
 * @return Index of new vertex in geometry.
 */
size_t qv_vertices_store_set_y_face_vertex(qv_vertices_store* store, size_t x, size_t y, size_t z,
                                           const qv_vec3* vertex);

/**
 * @brief Z face vertex setter.
 * Appends vertex to store's geometry and saves its index in store's indices.
 * If vertex already exists it will be overwritten.
 * @param store Initialized qv_vertices_store instance.
 * @param x X index of face.
 * @param y Y index of face.
 * @param z Z index of face.
 * @param vertex Vertex position to be set.
 * @return Index of new vertex in geometry.
 */
size_t qv_vertices_store_set_z_face_vertex(qv_vertices_store* store, size_t x, size_t y, size_t z,
                                           const qv_vec3* vertex);

/**
 * @brief Cube's face vertex setter.
 * Appends vertex to store's geometry and saves its index in store's indices.
 * If vertex already exists it will be overwritten.
 * @param store Initialized qv_vertices_store instance.
 * @param x X index of cube.
 * @param y Y index of cube.
 * @param z Z index of cube.
 * @param type One of 6 possible cube's face.
 * @param vertex Vertex position to be set.
 * @return Index of new vertex in geometry.
 */
size_t qv_vertices_store_set_face_vertex(qv_vertices_store* store, size_t x, size_t y, size_t z,
                                         qv_face_type type, const qv_vec3* vertex);

/**
 * @brief Internal cube's vertex getter.
 * @param store Initialized qv_vertices_store instance.
 * @param x X index of cube.
 * @param y Y index of cube.
 * @param z Z index of cube.
 * @param vertex Pointer to 3D vector where vertex position will be stored.
 * @param index Pointer to integer where vertex index will be stored.
 * @return 1 if vertex was found, 0 otherwise.
 */
int qv_vertices_store_get_internal_vertex(const qv_vertices_store* store, size_t x, size_t y, size_t z,
                                           qv_vec3* vertex, size_t* index);

/**
 * @brief X faces's vertex getter.
 * @param store Initialized qv_vertices_store instance.
 * @param x X index of face.
 * @param y Y index of face.
 * @param z Z index of face.
 * @param vertex Pointer to 3D vector where vertex position will be stored.
 * @param index Pointer to integer where vertex index will be stored.
 * @return 1 if vertex was found, 0 otherwise.
 */
int qv_vertices_store_get_x_face_vertex(const qv_vertices_store* store, size_t x, size_t y, size_t z,
                                         qv_vec3* vertex, size_t* index);

/**
 * @brief Y faces's vertex getter.
 * @param store Initialized qv_vertices_store instance.
 * @param x X index of face.
 * @param y Y index of face.
 * @param z Z index of face.
 * @param vertex Pointer to 3D vector where vertex position will be stored.
 * @param index Pointer to integer where vertex index will be stored.
 * @return 1 if vertex was found, 0 otherwise.
 */
int qv_vertices_store_get_y_face_vertex(const qv_vertices_store* store, size_t x, size_t y, size_t z,
                                         qv_vec3* vertex, size_t* index);

/**
 * @brief Z faces's vertex getter.
 * @param store Initialized qv_vertices_store instance.
 * @param x X index of face.
 * @param y Y index of face.
 * @param z Z index of face.
 * @param vertex Pointer to 3D vector where vertex position will be stored.
 * @param index Pointer to integer where vertex index will be stored.
 * @return 1 if vertex was found, 0 otherwise.
 */
int qv_vertices_store_get_z_face_vertex(const qv_vertices_store* store, size_t x, size_t y, size_t z,
                                         qv_vec3* vertex, size_t* index);

/**
 * @brief Cube's face vertex getter.
 * @param store Initialized qv_vertices_store instance.
 * @param x X index of cube.
 * @param y Y index of cube.
 * @param z Z index of cube.
 * @param type One of 6 possible cube's faces.
 * @param vertex Pointer to 3D vector where vertex position will be stored.
 * @param index Pointer to integer where vertex index will be stored.
 * @return 1 if vertex was found, 0 otherwise.
 */
int qv_vertices_store_get_face_vertex(const qv_vertices_store* store, size_t x, size_t y, size_t z,
                                       qv_face_type type, qv_vec3* vertex, size_t* index);


/**
 * @brief Appends index to store's geometry.
 * @param store Initialized qv_vertices_store instance.
 * @param idx Vertex index to be appended.
 */
void qv_vertices_store_append_index(qv_vertices_store* store, size_t idx);


#ifdef __cplusplus
}
#endif

#endif //QVORTLIB_VERTICES_STORE_H
