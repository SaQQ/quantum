/**
 * @file Example program using qvortlib.
 * It is reading binary data file, extracting vortices and showing some calculated statistics.
 * Also generated vortex vertices are exported in textual format to the hardcoded file geometry.txt.
 */

#include <stdio.h>

#include <qvortlib/grid_loader.h>
#include <qvortlib/vertices_store.h>
#include <qvortlib/extractor.h>
#include <qvortlib/statistics.h>
#include <qvortlib/exporter.h>

void usage() {
    fprintf(stderr, "USAGE: ./qvort data.bin\n");
}

int main(int argc, const char* argv[]) {
    if (argc != 2) {
        usage();
        return 1;
    }

    qv_grid grid;
    qv_read_binary_file(argv[1], &grid, 0);

    printf("Successfully read binary file \"%s\"\n", argv[1]);
    printf("Grid dimensions: %lu x %lu x %lu\n", grid.x_size, grid.y_size, grid.z_size);
    printf("Grid resolution: %.3f x %.3f x %.3f\n", grid.d_x, grid.d_y, grid.d_z);

    qv_vertices_store store;
    qv_vertices_store_init(&store, &grid);

    qv_generate_vertices(&store);

    qv_geometry* geometry = &store.geometry;
    printf("Generated vortex geometry: %lu vertices, %lu indices \n", geometry->vertex_count, geometry->index_count);

    size_t components_count = qv_split_vortices(geometry);
    printf("Independent vortices count: %lu\n", components_count);

    double total_length = qv_vortex_length(geometry);
    printf("Total vortices length: %.3f\n", total_length);

    qv_save_geometry("geometry.txt", geometry);

    qv_vertices_store_free(&store);

    qv_grid_free(&grid);

    return 0;
}
